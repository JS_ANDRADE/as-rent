package com.drida.asrent.presentation.travels;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.drida.asrent.model.contact.Person;
import com.drida.asrent.model.travel.TravelScreen;




import java.util.List;

public class TravelsScreenAdapter extends FragmentPagerAdapter {

    List<TravelScreen> screens;
    Person personEntity;

    public TravelsScreenAdapter(FragmentManager fm, Person personEntity) {

        super(fm);
        this.personEntity = personEntity;
    }

    public List<TravelScreen> getScreens() {
        return screens;
    }

    public void setScreens(List<TravelScreen> screens) {
        this.screens = screens;
    }

    @Override
    public Fragment getItem(int position) {
        TravelScreen screen = screens.get(position);

        if(screen.getType() == TravelScreen.TYPE_CURRENTS){
            //return new TravelsPersonFragmentBuilder(personEntity).build();
        }

        if(screen.getType() == TravelScreen.TYPE_HISTORY){
            //return new TravelsHistoryFragmentBuilder(personEntity).build();
        }

       throw  new RuntimeException("Exception en TravelScreenAdapter");
    }

    @Override
    public int getCount() {
        return screens == null ? 0 : screens.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return screens.get(position).getName();
    }
}
