package com.drida.asrent.presentation.travels;

import com.drida.asrent.base.presenter.BaseRxTravelPresenter;
import com.drida.asrent.model.contact.Person;
import com.drida.asrent.model.travel.Travel;
import com.drida.asrent.model.travel.TravelProvider;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

public class TravelsPresenter extends BaseRxTravelPresenter<TravelsView, List<Travel>> {



    @Inject
    public TravelsPresenter(TravelProvider travelProvider, EventBus eventBus) {
        super(travelProvider, eventBus);
    }

    public void load(boolean pullToRefresh, Person personEntity){

        subscribe(travelProvider.getTravelsOfUser("Prueba"), pullToRefresh);

    }

}
