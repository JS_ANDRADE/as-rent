package com.drida.asrent.presentation.travels;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.drida.asrent.R;
import com.drida.asrent.base.view.ListAdapter;
import com.drida.asrent.model.travel.Travel;
import com.hannesdorfmann.annotatedadapter.annotation.ViewField;
import com.hannesdorfmann.annotatedadapter.annotation.ViewType;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class TravelsAdapter extends ListAdapter<List<Travel>> implements TravelsAdapterBinder {


    public interface TravelClickedListener {
        public void onTravelClicked(TravelsAdapterHolders.TravelViewHolder vh, Travel travel);
    }

    @ViewType(
            layout = R.layout.list_travel_item,
            views = {
                    @ViewField(id = R.id.date, name = "date", type = TextView.class),
                    @ViewField(id = R.id.hour_start, name = "hour_start", type = TextView.class),
                    @ViewField(id = R.id.hour_finish, name = "hour_finish", type = TextView.class),
                    @ViewField(id = R.id.place_start, name = "place_start", type = TextView.class),
                    @ViewField(id = R.id.place_finish, name = "place_finish", type = TextView.class),
                    @ViewField(id = R.id.price, name = "price", type = TextView.class),
                    @ViewField(id = R.id.image_publisher, name = "image_publisher", type = ImageView.class),
                    @ViewField(id = R.id.name_publisher, name = "name_publisher", type = TextView.class)


            }) public final int travel = 0;


    private TravelClickedListener clickedListener;

    private Format format = new SimpleDateFormat("dd,MMM", Locale.getDefault());

    public TravelsAdapter(Context context, TravelClickedListener clickedListener) {
        super(context);
        this.clickedListener = clickedListener;

    }

    @Override
    public void bindViewHolder(final TravelsAdapterHolders.TravelViewHolder vh, int position) {

        final Travel travel = items.get(position);

        vh.date.setText(format.format(travel.getDate()));
        vh.hour_start.setText(travel.getHour_start());
        vh.hour_finish.setText(travel.getHour_finish());
        vh.place_start.setText(travel.getPlace_start());
        vh.place_finish.setText(travel.getPlace_finish());
        vh.name_publisher.setText(travel.getPublisher().getName());
        vh.image_publisher.setImageResource(travel.getPublisher().getImageRes());
        vh.price.setText(travel.getPrice());

        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickedListener.onTravelClicked(vh, travel);
            }
        });

    }


}
