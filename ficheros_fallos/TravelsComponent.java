package com.drida.asrent.presentation.travels;

import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.dagger.ShareAppComponent;
import com.drida.asrent.dagger.TravelModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {TravelModule.class, NavigationModule.class},
        dependencies = ShareAppComponent.class
)
public interface TravelsComponent {

    TravelsPresenter presenter();

    void inject(TravelsFragment fragment);

}
