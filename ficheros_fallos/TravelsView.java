package com.drida.asrent.presentation.travels;

import com.drida.asrent.base.view.BaseTravelView;
import com.drida.asrent.model.travel.Travel;

import java.util.List;

public interface TravelsView extends BaseTravelView<List<Travel>> {


}
