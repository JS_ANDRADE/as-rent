package com.drida.asrent;

import com.drida.asrent.MainActivity;
import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.dagger.ShareModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {ShareModule.class, NavigationModule.class}
)
public interface MainActivityComponent {

    void inject(MainActivity mainActivity);

}
