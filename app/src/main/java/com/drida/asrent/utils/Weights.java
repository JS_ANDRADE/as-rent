package com.drida.asrent.utils;

public class Weights {

    public static double transformGramsToKilos(Double grams){
        return grams/1000;
    }

    public static double transformKilosToGram(Double kilos){
        return kilos*1000;
    }

}
