package com.drida.asrent.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;


import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.editprofile.EditProfileActivity;
import com.drida.asrent.presentation.login.LoginActivity;
import com.drida.asrent.presentation.persondetails.PersonDetailsActivity;
import com.drida.asrent.presentation.post.PostActivity;
import com.drida.asrent.presentation.registred.RegistredActivity;
import com.drida.asrent.presentation.requesttrip.RequestTripActivity;
import com.drida.asrent.presentation.resultssearch.ResultsSearchActivity;
import com.drida.asrent.presentation.search.SearchActivity;
import com.drida.asrent.presentation.traveldetails.TravelsDetailsActivity;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

public class IntentStarter {

    public void showAuthentication(Context context) {
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    public void showRegistred(Context context) {
        context.startActivity(new Intent(context, RegistredActivity.class));
    }



    public void showTravelDetails(Context context, Travel travel) {
        Intent i = new Intent(context, TravelsDetailsActivity.class);
        i.putExtra(TravelsDetailsActivity.KEY_TRAVEL, travel);
        context.startActivity(i);
    }


    public void showPersonDetails(Context context, Person person){

        Intent i = new Intent(context, PersonDetailsActivity.class);
        i.putExtra(PersonDetailsActivity.KEY_PERSON, person);
        context.startActivity(i);

    }

    public void showSearch(Context context) {

        context.startActivity(new Intent(context, SearchActivity.class));
    }

    public void showSearchResults(Context context, SearchTravel searchTravel) {

        Intent i = new Intent(context,ResultsSearchActivity.class);
        i.putExtra(ResultsSearchActivity.KEY_SEARCH_TRAVEL, searchTravel);
        context.startActivity(i);

    }

    public void showPost(Context context) {
        context.startActivity(new Intent(context, PostActivity.class));
    }

    public void showAutocompletePlace(Activity context, int requestCode) {

        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(context);
            context.startActivityForResult(intent, requestCode);
        } catch (GooglePlayServicesRepairableException e) {

        } catch (GooglePlayServicesNotAvailableException e) {

        }


    }

    public void showRequestTrip(Activity context, Travel travel, int requestCode) {

        Intent i = new Intent(context, RequestTripActivity.class);

        i.putExtra(RequestTripActivity.KEY_TRAVEL, travel);

        context.startActivityForResult(i, requestCode);


    }

    public void showEditProfile(Context context, Person person){

        Intent i = new Intent(context, EditProfileActivity.class);

        i.putExtra(EditProfileActivity.KEY_PERSON, person);

        context.startActivity(i);

    }
}
