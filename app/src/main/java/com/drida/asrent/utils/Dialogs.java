package com.drida.asrent.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;

public class Dialogs {

    public static void showDialogSimple(Activity context, String title, String message){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(message)
                .setTitle(title);



        AlertDialog dialog = builder.create();

        dialog.show();

    }

}
