package com.drida.asrent.utils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Dates {


    public static final Calendar DATE_CURRENT = Calendar.getInstance();

    final static int MONTH = DATE_CURRENT.get(Calendar.MONTH);
    final static int DAY = DATE_CURRENT.get(Calendar.DAY_OF_MONTH);
    final static int YEAR = DATE_CURRENT.get(Calendar.YEAR);

    private static final SimpleDateFormat FORMAT_DATE_BASE = new SimpleDateFormat("dd/MM/yyyy");
    private static final SimpleDateFormat FORMAT_DATE_STRING = new SimpleDateFormat("yyyyMMdd");
    private static final Format FORMAT_DATE_STRING_VIEW = new SimpleDateFormat("E, MMM dd", Locale.getDefault());

    public static Calendar getDateCurrentBase() {

        String s = FORMAT_DATE_BASE.format(Calendar.getInstance().getTime());
        return convertStringBaseToCalendar(s);

    }

    public static void showDatePickerDialog(Context context, final EditText editText) {


        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {


                Calendar c = new GregorianCalendar(year, month, dayOfMonth);

                editText.setText(convertCalendarToDateFormatedBase(c));


            }
        }, YEAR, MONTH, DAY);

        datePickerDialog.show();


    }


    public static String transformDateStringToDateBase(String date) {

        if (date == null)
            return null;

        Calendar c = convertStringBaseToCalendar(date);
        return FORMAT_DATE_BASE.format(c.getTime());

    }

    public static String transformDateBaseToDateString(String date) {

        if (date == null)
            return null;

        Calendar c = convertStringBaseToCalendar(date);
        return FORMAT_DATE_STRING.format(c.getTime());
    }

    public static String convertCalendarToDateFormatedBase(Calendar date) {

        if (date == null)
            return null;

        String dateFormatted = FORMAT_DATE_BASE.format(convertCalendarToDate(date));

        return dateFormatted;

    }

    public static String convertCalendarToDateFormatedString(Calendar date) {

        if (date == null)
            return null;

        String dateFormatted = FORMAT_DATE_STRING.format(convertCalendarToDate(date));

        return dateFormatted;

    }


    public static String convertCalendarToDateFormatedView(Calendar date) {

        if (date == null)
            return null;

        String dateFormatted = FORMAT_DATE_STRING_VIEW.format(convertCalendarToDate(date));

        return dateFormatted;

    }


    public static Calendar convertDateStringToCalendar(String date) {

        if (date == null)
            return null;


        Log.d("ENTRO", "Entro converDateStringToCalendar");

        Calendar dateFormatted = Calendar.getInstance();

        try {
            dateFormatted.setTime(FORMAT_DATE_STRING.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateFormatted;

    }

    public static Calendar convertStringBaseToCalendar(String date) {

        if (date == null)
            return null;


        Calendar dateFormatted = Calendar.getInstance();

        try {
            dateFormatted.setTime(FORMAT_DATE_BASE.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateFormatted;

    }


    public static Date convertCalendarToDate(Calendar date) {

        if (date == null)
            return null;

        Date dateFormatted = date.getTime();

        return dateFormatted;

    }

    public static Calendar convertDateToCalendar(Date date) {

        if (date == null)
            return null;

        Calendar dateFormatted = Calendar.getInstance();
        dateFormatted.setTime(date);

        return dateFormatted;

    }

    public static String transformStringDate(Calendar date) {

        if (date == null)
            return null;

        String dateFormated = "";

        dateFormated = FORMAT_DATE_STRING.format(date.getTime());

        return dateFormated;


    }

    public static String transformStringDate(String date) {

        if (date == null)
            return null;

        String dateFormated = "";

        try {
            Date dateAux = FORMAT_DATE_BASE.parse(date);
            dateFormated = FORMAT_DATE_STRING.format(dateAux);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateFormated;

    }

    public static String transformDateStringtoDateFormated(String date) {


        if (date == null)
            return null;

        Log.d("ENTRO", "Entro transformDateStrinToDateFormatted");

        String dateFormated = "";

        Calendar c = convertDateStringToCalendar(date);

        if (c == null) {
            Log.d("ENTRO", "ENtro c---- null");
        }

        dateFormated = FORMAT_DATE_STRING_VIEW.format(c.getTime());

        return dateFormated;

    }

    public static int calculateAge(Calendar birthdate) {

        if (birthdate == null)
            return 0;

        int dIni = Integer.parseInt(FORMAT_DATE_STRING.format(convertCalendarToDate(birthdate)));
        int dEnd = Integer.parseInt(FORMAT_DATE_STRING.format(Calendar.getInstance().getTime()));
        int age = (dEnd - dIni) / 10000;
        return age;


    }

    public static String joinDates(String date1, String date2) {


        if (date1 == null || date2 == null) {
            return null;
        }

        Log.d("ENTRO", "Entro joinDates");


        if (date1.equals(date2)) {
            return transformDateStringtoDateFormated(date1);


        } else {
            return transformDateStringtoDateFormated(date1) + " - " + transformDateStringtoDateFormated(date2);
        }


    }


    public static String getDateStringCurrentDate() {

        Calendar c = Calendar.getInstance();

        String s = "";

        s = FORMAT_DATE_STRING.format(c.getTime());

        return s;

    }


    public static boolean isDateAvailable(String date, String hour) {

        if (date == null || hour == null)
            return false;

        Date current = getDateCurrentBase().getTime();
        Date calendar = null;
        try {
            calendar = FORMAT_DATE_STRING.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (calendar.compareTo(current) < 0) {
            return false;
        } else if (calendar.compareTo(current) == 0) {
            return Hours.isHourAvailable(hour);

        } else {
            return true;
        }


    }
}
