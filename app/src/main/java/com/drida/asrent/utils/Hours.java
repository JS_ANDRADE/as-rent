package com.drida.asrent.utils;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.widget.EditText;
import android.widget.TimePicker;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Hours {

    private static final String CERO = "0";
    private static final String TWO_POINTS = ":";

    public static final Calendar c = Calendar.getInstance();

    static final int hour = c.get(Calendar.HOUR_OF_DAY);
    static final int minute = c.get(Calendar.MINUTE);

    private static final SimpleDateFormat FORMAT_HOUR = new SimpleDateFormat("HH:mm");

    public static void showTimePickerDialog(Activity context, EditText editText) {


        TimePickerDialog obtainHour = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String houtFormated = (hourOfDay < 10) ? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutesFormated = (minute < 10) ? String.valueOf(CERO + minute) : String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario

                //Muestro la hora con el formato deseado
                editText.setText(houtFormated + TWO_POINTS + minutesFormated);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hour, minute, true);

        obtainHour.show();

    }

    public static Date getHourCurrent() {

        Date date = new Date();

        String dateString = FORMAT_HOUR.format(date);

        Date dateFormated = null;

        try {
            dateFormated = FORMAT_HOUR.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateFormated;

    }

    public static String converDateToHourString(Date hour) {

        if (hour == null)
            return null;

        return FORMAT_HOUR.format(hour);

    }

    public static Date converHourStringToDate(String hour) {


        if (hour == null)
            return null;

        Date date = null;

        try {
            date = FORMAT_HOUR.parse(hour);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static boolean isHourAvailable(String hour) {


        Date currentHour = getHourCurrent();
        Date hourTravel = Hours.converHourStringToDate(hour);
        return hourTravel.compareTo(currentHour) > 0;

    }

}
