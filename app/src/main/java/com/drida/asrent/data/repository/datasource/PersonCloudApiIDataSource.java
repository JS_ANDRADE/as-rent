package com.drida.asrent.data.repository.datasource;

import com.drida.asrent.data.cloud.CloudApi;
import com.drida.asrent.data.entity.PersonEntity;
import com.drida.asrent.data.repository.datasource.core.DataSource;
import com.drida.asrent.data.repository.datasource.core.IDataSource;
import com.drida.asrent.data.specification.Specification;

import java.util.List;

import io.reactivex.Observable;


public  class PersonCloudApiIDataSource extends DataSource {

    private final CloudApi cloudApi;

    public PersonCloudApiIDataSource(CloudApi cloudApi) {

        this.cloudApi = cloudApi;

    }

    @Override
    public Observable<List<PersonEntity>> queryPerson(Specification specification) {
        return cloudApi.queryPerson(specification);
    }

    @Override
    public Observable<PersonEntity> updatePerson(PersonEntity person) {
        return cloudApi.updatePerson(person);
    }
}
