package com.drida.asrent.data.repository.datasource;

import com.drida.asrent.data.cloud.CloudImpl;
import com.drida.asrent.data.repository.datasource.core.IDataSource;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;

public class AccountDataSourceFactory {

    FirebaseAuth auth;
    FirebaseDatabase database;

    @Inject
    public AccountDataSourceFactory(FirebaseAuth auth, FirebaseDatabase database) {
        this.auth = auth;
        this.database = database;
    }

    public IDataSource createDataSource() {

        CloudImpl cloud = new CloudImpl(auth, database);

        return new AccountCloudApiIDataSource(cloud);
    }


}
