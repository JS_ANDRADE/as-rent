package com.drida.asrent.data.repository;

import com.drida.asrent.data.entity.TravelEntity;
import com.drida.asrent.data.specification.Specification;
import com.drida.asrent.domain.model.travel.RequestTravel;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.domain.model.travel.Travel;

import java.util.List;

import io.reactivex.Observable;


public interface ITravelRepository {

    Observable<Travel> addTravel(Travel travel);

    Observable<List<Travel>> addTravel(List<Travel> travels);

    Observable<Travel> updateTravel(Travel travel);

    Observable<Travel> removeTravel(Travel travel);

    Observable<List<Travel>> removeTravel(Specification specification);

    Observable<List<Travel>> queryTravel(Specification specification);

    Observable<List<Travel>> queryTravelBySearch(SearchTravel searchTravel);

    Observable<Travel> requestTravel(RequestTravel requestTravel);

    Observable<List<Travel>> queryTravelsByApplicant(String uidApplicant);

    Observable<List<Travel>> queryTravelsByPublisher(String uidPublisher);

}
