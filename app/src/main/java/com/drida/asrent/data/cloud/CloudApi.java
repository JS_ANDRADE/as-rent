package com.drida.asrent.data.cloud;

import com.drida.asrent.data.entity.TravelEntity;
import com.drida.asrent.data.specification.Specification;
import com.drida.asrent.data.entity.AccountEntity;
import com.drida.asrent.data.entity.PersonEntity;
import com.drida.asrent.domain.model.travel.RequestTravel;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.domain.model.travel.Travel;
import com.google.firebase.auth.AuthCredential;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.Single;


public interface CloudApi {


    Observable<AccountEntity> logInUser(String email, String password);

    Observable<AccountEntity> logInUserFacebook(AuthCredential credential);

    Observable<AccountEntity> registrerUser(PersonEntity item);

    Observable<AccountEntity> logOutUser();

    AccountEntity getCurrentUser();

    boolean isUserAuthenticated();

    /*--------------------------------------------------*/

    Observable<PersonEntity> addPerson(PersonEntity item);

    Observable<List<PersonEntity>> addPerson(List<PersonEntity> items);

    Observable<PersonEntity> updatePerson(PersonEntity item);

    Observable<PersonEntity> removePerson(PersonEntity item);

    Observable<List<PersonEntity>> removePerson(Specification specification);

    Observable<List<PersonEntity>> queryPerson(Specification specification);


    /*--------------------------------------------------*/

    Observable<TravelEntity> addTravel(TravelEntity item);

    Observable<List<TravelEntity>> addTravel(List<TravelEntity> items);

    Observable<TravelEntity> updateTravel(TravelEntity item);

    Observable<TravelEntity> removeTravel(TravelEntity item);

    Observable<List<TravelEntity>> removeTravel(Specification specification);

    Observable<List<TravelEntity>> queryTravel(Specification specification);

    Observable<List<TravelEntity>> queryTravelsByApplicant(String uidApplicant);

    Observable<List<TravelEntity>> queryTravelsByPublisher(String uidPublisher);

    Observable<List<TravelEntity>> queryTravelBySearch(SearchTravel searchTravel);


    Observable<TravelEntity> requestTravel(RequestTravel requestTravel);



}
