package com.drida.asrent.data.repository;

import com.drida.asrent.data.entity.PersonEntity;
import com.drida.asrent.data.repository.datasource.PersonDataSourceFactory;
import com.drida.asrent.data.repository.datasource.core.IDataSource;
import com.drida.asrent.data.repository.datasource.mapper.PersonToPersonEntityMapper;
import com.drida.asrent.data.specification.Specification;
import com.drida.asrent.domain.model.contact.Person;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;


public class PersonRepository implements IPersonRepository {

    private final PersonToPersonEntityMapper personToPersonEntityMapper;
    private final IDataSource dataSource;

    @Inject
    public PersonRepository(PersonDataSourceFactory personDataSourceFactory,
                            PersonToPersonEntityMapper personToPersonEntityMapper) {
        this.personToPersonEntityMapper = personToPersonEntityMapper;
        this.dataSource = personDataSourceFactory.createDataSource();
    }

    @Override
    public Observable<List<Person>> queryPerson(Specification specification) {

        return dataSource.queryPerson(specification).map(personToPersonEntityMapper::reverseMap);
    }

    @Override
    public Observable<Person> addPerson(Person person) {
        PersonEntity personEntity = personToPersonEntityMapper.map(person);
        return dataSource.addPerson(personEntity).map(personToPersonEntityMapper::reverseMap);
    }

    @Override
    public Observable<List<Person>> addPerson(List<Person> persons) {
        List<PersonEntity> personEntity = personToPersonEntityMapper.map(persons);
        return dataSource.addPerson(personEntity).map(personToPersonEntityMapper::reverseMap);
    }

    @Override
    public Observable<Person> updatePerson(Person person) {
        PersonEntity personAux = personToPersonEntityMapper.map(person);
        return dataSource.updatePerson(personAux).map(personToPersonEntityMapper::reverseMap);
    }

    @Override
    public Observable<Person> removePerson(Person person) {
        PersonEntity personEntity = personToPersonEntityMapper.map(person);
        return dataSource.removePerson(personEntity).map(personToPersonEntityMapper::reverseMap);
    }

    @Override
    public Observable<List<Person>> removePerson(Specification specification) {

        return dataSource.removePerson(specification).map(personToPersonEntityMapper::reverseMap);
    }
}
