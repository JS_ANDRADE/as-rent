package com.drida.asrent.data.repository.datasource.core;

import com.drida.asrent.data.entity.AccountEntity;
import com.drida.asrent.data.entity.PersonEntity;
import com.drida.asrent.data.entity.TravelEntity;
import com.drida.asrent.data.specification.Specification;
import com.drida.asrent.domain.model.travel.RequestTravel;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.domain.model.travel.Travel;
import com.google.firebase.auth.AuthCredential;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class DataSource  implements IDataSource{


    @Override
    public Observable<AccountEntity> logInUser(String email, String password) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<AccountEntity> logInUserFacebook(AuthCredential credential) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<AccountEntity> logOutUser() {
        throw new UnsupportedOperationException();
    }

    @Override
    public AccountEntity getUserCurrent() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isUserAuhtenticated() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<AccountEntity> registredUser(PersonEntity item) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<TravelEntity> addTravel(TravelEntity travel) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<List<TravelEntity>> addTravel(List<TravelEntity> travels) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<TravelEntity> updateTravel(TravelEntity travel) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<TravelEntity> removeTravel(TravelEntity travel) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<List<TravelEntity>> removeTravel(Specification specification) {
        throw new UnsupportedOperationException();
    }

    @Override
    public  Observable<List<TravelEntity>> queryTravel(Specification specification) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<List<TravelEntity>> queryTravelBySearch(SearchTravel searchTravel) {
        throw  new UnsupportedOperationException();
    }

    @Override
    public Observable<List<TravelEntity>> queryTravelsByApplicant(String uidApplicant) {
        throw  new UnsupportedOperationException();
    }

    @Override
    public Observable<List<TravelEntity>> queryTravelsByPublisher(String uidPublisher) {
        throw  new UnsupportedOperationException();
    }

    @Override
    public Observable<TravelEntity> requestTravel(RequestTravel requesTravel) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<PersonEntity> addPerson(PersonEntity person) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<List<PersonEntity>> addPerson(List<PersonEntity> person) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<PersonEntity> updatePerson(PersonEntity person) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<PersonEntity> removePerson(PersonEntity person) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<List<PersonEntity>> removePerson(Specification specification) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<List<PersonEntity>> queryPerson(Specification specification) {
        throw new UnsupportedOperationException();
    }
}
