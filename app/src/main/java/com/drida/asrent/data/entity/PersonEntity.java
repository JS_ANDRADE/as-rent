package com.drida.asrent.data.entity;

public class PersonEntity {


    private String uid;
    private String name;
    private String surname;
    private String gender;
    private String birthdate;
    private String password;
    private int ratings;
    private int travels;
    private String facebook;
    private String email;
    private String phone;
    private boolean verified_email;
    private boolean verified_facebook;
    private boolean verified_phone;
    private String ocupation;
    private String member_since;
    private String description;
    private String location;



    public PersonEntity() {

    }

    public PersonEntity(String uid, String name, String surname, String gender, String birthdate, String password, int ratings, int travels, String facebook, String email, String phone, boolean verified_email, boolean verified_facebook, boolean verified_phone, String ocupation, String member_since, String description) {
        this.uid = uid;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.birthdate = birthdate;
        this.password = password;
        this.ratings = ratings;
        this.travels = travels;
        this.facebook = facebook;
        this.email = email;
        this.phone = phone;
        this.verified_email = verified_email;
        this.verified_facebook = verified_facebook;
        this.verified_phone = verified_phone;
        this.ocupation = ocupation;
        this.member_since = member_since;
        this.description = description;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRatings() {
        return ratings;
    }

    public void setRatings(int ratings) {
        this.ratings = ratings;
    }

    public int getTravels() {
        return travels;
    }

    public void setTravels(int travels) {
        this.travels = travels;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isVerified_email() {
        return verified_email;
    }

    public void setVerified_email(boolean verified_email) {
        this.verified_email = verified_email;
    }

    public boolean isVerified_facebook() {
        return verified_facebook;
    }

    public void setVerified_facebook(boolean verified_facebook) {
        this.verified_facebook = verified_facebook;
    }

    public boolean isVerified_phone() {
        return verified_phone;
    }

    public void setVerified_phone(boolean verified_phone) {
        this.verified_phone = verified_phone;
    }

    public String getOcupation() {
        return ocupation;
    }

    public void setOcupation(String ocupation) {
        this.ocupation = ocupation;
    }

    public String getMember_since() {
        return member_since;
    }

    public void setMember_since(String member_since) {
        this.member_since = member_since;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}

