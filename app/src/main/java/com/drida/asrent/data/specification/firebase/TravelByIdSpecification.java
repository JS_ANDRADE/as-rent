package com.drida.asrent.data.specification.firebase;

import com.drida.asrent.data.cloud.FirebaseReferences;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;

public class TravelByIdSpecification implements FirebaseSpecification {
    private String uidTravel;

    public TravelByIdSpecification(String uidTravel) {
        this.uidTravel = uidTravel;
    }

    @Override
    public Query toFirebaseQuery() {
        Query query = FirebaseDatabase.getInstance()
                .getReference(FirebaseReferences.TRAVELS_REFERENCE)
                .orderByChild(FirebaseReferences.TRAVELS_UID_REFERENCE)
                .equalTo(uidTravel);

        return query;
    }


}
