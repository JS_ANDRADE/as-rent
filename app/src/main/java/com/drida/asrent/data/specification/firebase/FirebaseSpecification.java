package com.drida.asrent.data.specification.firebase;

import com.drida.asrent.data.specification.Specification;
import com.google.firebase.database.Query;

import java.util.List;

public interface FirebaseSpecification extends Specification {
   Query toFirebaseQuery();
}


