package com.drida.asrent.data.entity;

public class TravelRequestedEntity {

    private String uid;
    private String uid_travel;
    private String uid_publisher;
    private String uid_applicant;

    public TravelRequestedEntity() {
    }

    public TravelRequestedEntity(String uid, String uid_travel, String uid_publisher, String uid_applicant) {
        this.uid = uid;
        this.uid_travel = uid_travel;
        this.uid_publisher = uid_publisher;
        this.uid_applicant = uid_applicant;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid_travel() {
        return uid_travel;
    }

    public void setUid_travel(String uid_travel) {
        this.uid_travel = uid_travel;
    }

    public String getUid_publisher() {
        return uid_publisher;
    }

    public void setUid_publisher(String uid_publisher) {
        this.uid_publisher = uid_publisher;
    }

    public String getUid_applicant() {
        return uid_applicant;
    }

    public void setUid_applicant(String uid_applicant) {
        this.uid_applicant = uid_applicant;
    }
}
