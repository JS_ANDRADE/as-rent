package com.drida.asrent.data.repository.datasource.mapper;

import com.drida.asrent.data.entity.PersonEntity;
import com.drida.asrent.domain.model.contact.Person;

import javax.inject.Inject;

public class PersonToPersonEntityMapper extends Mapper<Person, PersonEntity> {


    @Inject
    public PersonToPersonEntityMapper(){

    }

    @Override
    public PersonEntity map(Person value) {

        PersonEntity personEntity = new PersonEntity();

        personEntity.setUid(value.getId());
        personEntity.setName(value.getName());
        personEntity.setSurname(value.getSurname());
        personEntity.setGender(value.getGender());
        personEntity.setBirthdate(value.getBirthdate());
        personEntity.setEmail(value.getEmail());
        personEntity.setPassword(value.getPassword());
        personEntity.setDescription(value.getDescription());
        personEntity.setFacebook(value.getFacebook());
        personEntity.setMember_since(value.getMemberSince());
        personEntity.setOcupation(value.getOcupation());
        personEntity.setPhone(value.getPhone());
        personEntity.setRatings(value.getRatings());
        personEntity.setTravels(value.getTravels());
        personEntity.setVerified_email(value.isVerifiedEmail());
        personEntity.setVerified_facebook(value.isVerifiedFacebook());
        personEntity.setVerified_phone(value.isVerifiedPhone());
        personEntity.setLocation(value.getLocation());

        return personEntity;
    }

    @Override
    public Person reverseMap(PersonEntity value) {
        Person person = new Person();

        person.setId(value.getUid());
        person.setName(value.getName());
        person.setSurname(value.getSurname());
        person.setGender(value.getGender());
        person.setBirthdate(value.getBirthdate());
        person.setEmail(value.getEmail());
        person.setPassword(value.getPassword());
        person.setDescription(value.getDescription());
        person.setFacebook(value.getFacebook());
        person.setMemberSince(value.getMember_since());
        person.setOcupation(value.getOcupation());
        person.setPhone(value.getPhone());
        person.setRatings(value.getRatings());
        person.setTravels(value.getTravels());
        person.setVerifiedEmail(value.isVerified_email());
        person.setVerifiedFacebook(value.isVerified_facebook());
        person.setVerifiedPhone(value.isVerified_phone());
        person.setLocation(value.getLocation());

        return person;
    }
}
