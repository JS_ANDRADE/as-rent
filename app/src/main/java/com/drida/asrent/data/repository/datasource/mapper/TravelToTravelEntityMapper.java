package com.drida.asrent.data.repository.datasource.mapper;

import android.util.Log;

import com.drida.asrent.data.entity.PersonEntity;
import com.drida.asrent.data.entity.TravelEntity;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.travel.Travel;

import javax.inject.Inject;

public class TravelToTravelEntityMapper extends Mapper<Travel, TravelEntity> {


    private PersonToPersonEntityMapper personToPersonEntityMapper;

    @Inject
    public TravelToTravelEntityMapper(PersonToPersonEntityMapper personToPersonEntityMapper) {
        this.personToPersonEntityMapper = personToPersonEntityMapper;
    }

    @Override
    public TravelEntity map(Travel value) {
        TravelEntity travelEntity = new TravelEntity();

        travelEntity.setUid(value.getId());
        travelEntity.setDate_departure(value.getDateDeparture());
        travelEntity.setAvailable_space(value.getAvailableSpace());
        travelEntity.setPrice_weight(value.getPriceWeight());
        travelEntity.setLocation_origin(value.getLocationOrigin());
        travelEntity.setLocation_destination(value.getLocationDestination());
        travelEntity.setHour_departure(value.getHourDeparture());
        travelEntity.setHour_arrival(value.getHourArrival());
        travelEntity.setDescription(value.getDescription());
        travelEntity.setUid_publisher(value.getIdPublisher());
        travelEntity.setType_weight(value.getTypeWeight());
        travelEntity.setUid_applicant(value.getIdApplicant());
        travelEntity.setSpace_requested(value.getSpaceRequested());
        travelEntity.setDate_arrival(value.getDateArrival());

        return travelEntity;
    }

    @Override
    public Travel reverseMap(TravelEntity value) {
        Travel travel = new Travel();

        Log.d("ENTRO", "Entro reverseMap estado de uid TravelEntity: " + value.getUid());

        travel.setId(value.getUid());
        travel.setDateDeparture(value.getDate_departure());
        travel.setAvailableSpace(value.getAvailable_space());
        travel.setPriceWeight(value.getPrice_weight());
        travel.setLocationOrigin(value.getLocation_origin());
        travel.setLocationDestination(value.getLocation_destination());
        travel.setHourDeparture(value.getHour_departure());
        travel.setHourArrival(value.getHour_arrival());
        travel.setDescription(value.getDescription());
        travel.setIdPublisher(value.getUid_publisher());
        travel.setTypeWeight(value.getType_weight());
        travel.setIdApplicant(value.getUid_applicant());
        travel.setSpaceRequested(value.getSpace_requested());
        travel.setDateArrival(value.getDate_arrival());

        Person publisher = new Person();
        if(value.getPublisher()!=null)
            publisher = personToPersonEntityMapper.reverseMap(value.getPublisher());

        travel.setPublisher(publisher);

        return travel;
    }
}
