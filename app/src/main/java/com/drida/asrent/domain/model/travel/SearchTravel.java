package com.drida.asrent.domain.model.travel;

import android.os.Parcel;
import android.os.Parcelable;

import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

@ParcelablePlease
public class SearchTravel implements Parcelable {

    String from;
    String to;
    String date;
    double weight;

    public SearchTravel() {
    }

    public SearchTravel(String from, String to, String date, double weight) {
        this.from = from;
        this.to = to;
        this.date = date;
        this.weight = weight;
    }

    protected SearchTravel(Parcel in) {
        from = in.readString();
        to = in.readString();
        date = in.readString();
        weight = in.readDouble();
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        SearchTravelParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<SearchTravel> CREATOR = new Creator<SearchTravel>() {
        public SearchTravel createFromParcel(Parcel source) {
            SearchTravel target = new SearchTravel();
            SearchTravelParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public SearchTravel[] newArray(int size) {
            return new SearchTravel[size];
        }
    };
}
