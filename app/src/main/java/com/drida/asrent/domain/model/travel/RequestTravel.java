package com.drida.asrent.domain.model.travel;

public class RequestTravel {

    String uid_applicant;
    String uid_publisher;
    String uid_travel;
    double space_requested;

    public RequestTravel() {
    }

    public String getUid_applicant() {
        return uid_applicant;
    }

    public void setUid_applicant(String uid_applicant) {
        this.uid_applicant = uid_applicant;
    }

    public String getUid_publisher() {
        return uid_publisher;
    }

    public void setUid_publisher(String uid_publisher) {
        this.uid_publisher = uid_publisher;
    }

    public String getUid_travel() {
        return uid_travel;
    }

    public void setUid_travel(String uid_travel) {
        this.uid_travel = uid_travel;
    }

    public double getSpace_requested() {
        return space_requested;
    }

    public void setSpace_requested(double space_requested) {
        this.space_requested = space_requested;
    }
}



