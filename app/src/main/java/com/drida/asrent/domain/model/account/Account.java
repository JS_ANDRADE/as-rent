package com.drida.asrent.domain.model.account;


import android.os.Parcel;
import android.os.Parcelable;

import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

@ParcelablePlease
public class Account implements Parcelable {

    String idUser;

    public Account() {
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        AccountParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<Account> CREATOR = new Creator<Account>() {
        public Account createFromParcel(Parcel source) {
            Account target = new Account();
            AccountParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public Account[] newArray(int size) {
            return new Account[size];
        }
    };
}
