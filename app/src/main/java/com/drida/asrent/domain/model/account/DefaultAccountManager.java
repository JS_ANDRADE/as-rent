package com.drida.asrent.domain.model.account;


import android.util.Log;

import com.drida.asrent.data.repository.AccountRepository;

import com.drida.asrent.domain.model.contact.Person;


import com.facebook.AccessToken;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;


public class DefaultAccountManager implements AccountManager {

    private final AccountRepository repository;

    @Inject
    public DefaultAccountManager(AccountRepository repository) {
        this.repository = repository;

    }

    @Override
    public Observable<Account> doLogin(AuthCredentials credentials) {

        return repository.logInUser(credentials.getEmail(), credentials.getPassword());


    }

    @Override
    public Observable<Account> doLoginFacebook(AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());

        return repository.logInUserFacebook(credential);


    }

    @Override
    public Observable<Account> doRegistred(Person person) {

        return repository.registredUser(person);
    }

    @Override
    public Observable<Account> doLogOut() {


        Log.d("ENTRO", "ENTRO doLogOut DefaultAccountManager");
        return repository.logOutUser();
    }

    @Override
    public Account getCurrentAccount() {

        return repository.getUserCurrent();
    }


    @Override
    public boolean isUserAuthenticated() {


        return repository.isUserAuhtenticated();

    }
}
