package com.drida.asrent.domain.model.contact;


import android.os.Parcel;
import android.os.Parcelable;


import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

@ParcelablePlease
public class Person implements Parcelable {

    String id;
    String name;
    String surname;
    String gender;
    String birthdate;
    String email;
    String password;
    int ratings;
    int travels;
    String facebook;
    boolean  verifiedFacebook;
    boolean verifiedEmail;
    String ocupation;
    String location;
    String memberSince;
    String description;
    String phone;
    boolean verifiedPhone;


    int imageRes;



    public Person() {
    }


    public Person(String id, String name, String surname, String gender, String birthdate, String email, String password, int ratings, int travels, String facebook, boolean verifiedFacebook, boolean verifiedEmail, String ocupation, String location, String memberSince, String description, String phone, int imageRes) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.birthdate = birthdate;
        this.email = email;
        this.password = password;
        this.ratings = ratings;
        this.travels = travels;
        this.facebook = facebook;
        this.verifiedFacebook = verifiedFacebook;
        this.verifiedEmail = verifiedEmail;
        this.ocupation = ocupation;
        this.location = location;
        this.memberSince = memberSince;
        this.description = description;
        this.phone = phone;
        this.imageRes = imageRes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRatings() {
        return ratings;
    }

    public void setRatings(int ratings) {
        this.ratings = ratings;
    }

    public int getTravels() {
        return travels;
    }

    public void setTravels(int travels) {
        this.travels = travels;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public boolean isVerifiedFacebook() {
        return verifiedFacebook;
    }

    public void setVerifiedFacebook(boolean verifiedFacebook) {
        this.verifiedFacebook = verifiedFacebook;
    }

    public boolean isVerifiedEmail() {
        return verifiedEmail;
    }

    public void setVerifiedEmail(boolean verifiedEmail) {
        this.verifiedEmail = verifiedEmail;
    }

    public String getOcupation() {
        return ocupation;
    }

    public void setOcupation(String ocupation) {
        this.ocupation = ocupation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(String memberSince) {
        this.memberSince = memberSince;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isVerifiedPhone() {
        return verifiedPhone;
    }

    public void setVerifiedPhone(boolean verifiedPhone) {
        this.verifiedPhone = verifiedPhone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        PersonParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        public Person createFromParcel(Parcel source) {
            Person target = new Person();
            PersonParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
}
