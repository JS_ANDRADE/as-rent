package com.drida.asrent.domain.model.travel;

import android.util.Log;

import com.drida.asrent.data.repository.TravelRepository;
import com.drida.asrent.data.specification.firebase.TravelByIdSpecification;
import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.account.NotAuthenticatedException;
import com.drida.asrent.utils.Dates;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.observers.BlockingBaseObserver;
import io.reactivex.internal.util.AppendOnlyLinkedArrayList;


public class TravelProvider {

    private AccountManager accountManager;
    private TravelRepository repository;


    @Inject
    public TravelProvider(AccountManager accountManager, @Named("repository_travel") TravelRepository repository) {
        this.accountManager = accountManager;
        this.repository = repository;
    }

    public Observable<List<Travel>> getTravelsOfApplicant(final String uidUser) {

        return Observable.defer((Callable<ObservableSource<? extends List<Travel>>>) () -> {
            Observable o = checkExceptions();
            if (o != null) {
                return o;
            }

            return repository.queryTravelsByApplicant(uidUser);

        });


    }


    public Observable<List<Travel>> getTravelsOfPublisher(final String uidUser) {


        return Observable.defer((Callable<ObservableSource<? extends List<Travel>>>) () -> {
            Observable o = checkExceptions();
            if (o != null) {
                return o;
            }

            return repository.queryTravelsByPublisher(uidUser);


        });


    }


    public Observable<List<Travel>> getTravelsOfSearch(SearchTravel searchTravel) {

        return repository.queryTravelBySearch(searchTravel);

    }

    public Observable<Travel> getTravel(final String idTravel) {


        return Observable.create(emitter -> {
            repository.queryTravel(new TravelByIdSpecification(idTravel))
                    .subscribe(new BlockingBaseObserver<List<Travel>>() {
                        @Override
                        public void onNext(List<Travel> travels) {

                            emitter.onNext(travels.get(0));
                            emitter.onComplete();
                        }

                        @Override
                        public void onError(Throwable e) {
                            emitter.onError(e);
                        }
                    });
        });


    }

    public Observable<Travel> requestTravel(RequestTravel travel) {


        return repository.requestTravel(travel);

    }

    public Observable<Travel> postTravel(Travel travel) {

        return repository.addTravel(travel);

    }



    private Observable checkExceptions() {
        if (!accountManager.isUserAuthenticated()) {
            Log.d("ENTRO", "Entro checkExceptions() en TraveProvider" + accountManager.isUserAuthenticated());

            return Observable.error(new NotAuthenticatedException());
        }

        return null;

    }

}
