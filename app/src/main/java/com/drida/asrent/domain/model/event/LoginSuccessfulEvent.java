package com.drida.asrent.domain.model.event;


import com.drida.asrent.domain.model.account.Account;

public class LoginSuccessfulEvent {

    private Account account;

    public LoginSuccessfulEvent(Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

}
