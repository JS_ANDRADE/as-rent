package com.drida.asrent.domain.model.travel;

import android.os.Parcel;
import android.os.Parcelable;


import com.drida.asrent.domain.model.contact.Person;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;


@ParcelablePlease
public class Travel implements Parcelable {

    String id;
    String dateDeparture;
    String dateArrival;
    double availableSpace;
    double priceWeight;
    String locationOrigin;
    String locationDestination;
    String hourDeparture;
    String hourArrival;
    String description;
    String idPublisher;
    String idApplicant;
    String typeWeight;
    double spaceRequested;

    Person publisher;
    Person applicant;

    public Travel() {

    }

    public Travel(String id, String dateDeparture, double availableSpace, double priceWeight, String locationOrigin, String locationDestination, String hourDeparture, String hourArrival, String description, String idPublisher, String typeWeight, Person publisher) {
        this.id = id;
        this.dateDeparture = dateDeparture;
        this.availableSpace = availableSpace;
        this.priceWeight = priceWeight;
        this.locationOrigin = locationOrigin;
        this.locationDestination = locationDestination;
        this.hourDeparture = hourDeparture;
        this.hourArrival = hourArrival;
        this.description = description;
        this.idPublisher = idPublisher;
        this.typeWeight = typeWeight;
        this.publisher = publisher;
    }

    public String getDateArrival() {
        return dateArrival;
    }

    public void setDateArrival(String dateArrival) {
        this.dateArrival = dateArrival;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(String dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public double getAvailableSpace() {
        return availableSpace;
    }

    public void setAvailableSpace(double availableSpace) {
        this.availableSpace = availableSpace;
    }

    public double getPriceWeight() {
        return priceWeight;
    }

    public void setPriceWeight(double priceWeight) {
        this.priceWeight = priceWeight;
    }

    public String getLocationOrigin() {
        return locationOrigin;
    }

    public void setLocationOrigin(String locationOrigin) {
        this.locationOrigin = locationOrigin;
    }

    public String getLocationDestination() {
        return locationDestination;
    }

    public void setLocationDestination(String locationDestination) {
        this.locationDestination = locationDestination;
    }

    public String getHourDeparture() {
        return hourDeparture;
    }

    public void setHourDeparture(String hourDeparture) {
        this.hourDeparture = hourDeparture;
    }

    public String getHourArrival() {
        return hourArrival;
    }

    public void setHourArrival(String hourArrival) {
        this.hourArrival = hourArrival;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdPublisher() {
        return idPublisher;
    }

    public void setIdPublisher(String idPublisher) {
        this.idPublisher = idPublisher;
    }

    public Person getPublisher() {
        return publisher;
    }


    public void setTypeWeight(String typeWeight) {
        this.typeWeight = typeWeight;
    }

    public String getTypeWeight() {
        return typeWeight;
    }

    public void setPublisher(Person publisher) {
        this.publisher = publisher;
    }

    public String getIdApplicant() {
        return idApplicant;
    }

    public void setIdApplicant(String idApplicant) {
        this.idApplicant = idApplicant;
    }

    public Person getApplicant() {
        return applicant;
    }

    public void setApplicant(Person applicant) {
        this.applicant = applicant;
    }

    public double getSpaceRequested() {
        return spaceRequested;
    }

    public void setSpaceRequested(double spaceRequested) {
        this.spaceRequested = spaceRequested;
    }

    public static Creator<Travel> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        TravelParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<Travel> CREATOR = new Creator<Travel>() {
        public Travel createFromParcel(Parcel source) {
            Travel target = new Travel();
            TravelParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public Travel[] newArray(int size) {
            return new Travel[size];
        }
    };
}
