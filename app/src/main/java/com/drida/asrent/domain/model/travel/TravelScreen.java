package com.drida.asrent.domain.model.travel;

import android.os.Parcel;
import android.os.Parcelable;

import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

@ParcelablePlease
public class TravelScreen implements Parcelable {


    public static final int TYPE_PUBLISHED = 0;
    public static final int TYPE_REQUESTED = 1;

    int type;
    String name;

    public TravelScreen() {
    }

    public TravelScreen(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        TravelScreenParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<TravelScreen> CREATOR = new Creator<TravelScreen>() {
        public TravelScreen createFromParcel(Parcel source) {
            TravelScreen target = new TravelScreen();
            TravelScreenParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public TravelScreen[] newArray(int size) {
            return new TravelScreen[size];
        }
    };
}
