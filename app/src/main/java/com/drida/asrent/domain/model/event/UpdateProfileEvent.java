package com.drida.asrent.domain.model.event;

import com.drida.asrent.domain.model.contact.Person;

public class UpdateProfileEvent {

    private Person person;

    public UpdateProfileEvent(Person person){
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }
}
