package com.drida.asrent.domain.model.account;

import com.drida.asrent.domain.model.contact.Person;

import com.facebook.AccessToken;
import com.google.firebase.auth.AuthCredential;

import io.reactivex.Observable;


public interface AccountManager {
    Observable<Account> doLogin(AuthCredentials credentials);

    Observable<Account> doLoginFacebook(AccessToken accessToken);

    Observable<Account> doRegistred(Person person);

    Observable<Account> doLogOut();

    Account getCurrentAccount();

    boolean isUserAuthenticated();

}
