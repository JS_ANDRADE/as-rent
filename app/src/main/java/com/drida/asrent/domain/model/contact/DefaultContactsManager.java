package com.drida.asrent.domain.model.contact;

import android.util.Log;

import com.drida.asrent.data.repository.PersonRepository;
import com.drida.asrent.data.specification.firebase.PersonByIdSpecification;
import com.drida.asrent.domain.model.account.Account;
import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.account.NotAuthenticatedException;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.internal.observers.BlockingBaseObserver;

public class DefaultContactsManager implements ContactsManager {

    AccountManager accountManager;
    PersonRepository repository;

    @Inject
    public DefaultContactsManager(AccountManager accountManager, @Named("repository_person") PersonRepository repository) {
        this.accountManager = accountManager;
        this.repository = repository;
    }


    @Override
    public Observable<Person> getPerson(String idPerson) {

        return Observable.defer(new Callable<ObservableSource<? extends Person>>() {
            @Override
            public ObservableSource<? extends Person> call() throws Exception {

                Observable o = checkExceptions();
                if (o != null) {
                    return o;
                }

                return repository.queryPerson(new PersonByIdSpecification(idPerson))
                        .map(s -> s.get(0));

            }
        });
    }

    @Override
    public Observable<Person> updatePerson(Person person) {
        return repository.updatePerson(person);
    }

    private Observable checkExceptions() {
        if (!accountManager.isUserAuthenticated()) {
            Log.d("ENTRO", "Entro checkExceptions() en TraveProvider" + accountManager.isUserAuthenticated());

            return Observable.error(new NotAuthenticatedException());
        }

        return null;

    }

}
