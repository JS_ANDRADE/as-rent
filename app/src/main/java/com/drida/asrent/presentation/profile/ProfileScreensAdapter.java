package com.drida.asrent.presentation.profile;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;


import com.drida.asrent.domain.model.contact.Person;

import com.drida.asrent.domain.model.contact.ProfileScreen;
import com.drida.asrent.presentation.profile.account.ProfileAccountFragmentBuilder;
import com.drida.asrent.presentation.profile.deatails.ProfileDetailsFragmentBuilder;


import java.util.List;

public class ProfileScreensAdapter extends FragmentStatePagerAdapter {

    private List<ProfileScreen> screens;
    private Person person;


    public ProfileScreensAdapter(FragmentManager fm, Person person) {
        super(fm);
        Log.d("ENTRO", "Entro constructor ProfileScreensAdapter");
        this.person = person;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public List<ProfileScreen> getScreens() {
        return screens;
    }

    public void setScreens(List<ProfileScreen> screens) {
        this.screens = screens;
    }

    @Override
    public Fragment getItem(int position) {

        ProfileScreen screen = screens.get(position);

        if (screen.getType() == ProfileScreen.TYPE_DETAILS) {

            return new ProfileDetailsFragmentBuilder(person).build();

        }

        if (screen.getType() == ProfileScreen.TYPE_ACCOUNT) {

            return new ProfileAccountFragmentBuilder(person).build();

        }

        throw new RuntimeException("Error Profile Screen Adapter");

    }

    @Override
    public int getCount() {
        return screens == null ? 0 : screens.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return screens.get(position).getName();
    }
}
