package com.drida.asrent.presentation.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.drida.asrent.ShareApplication;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.contact.ProfileScreen;
import com.drida.asrent.presentation.base.view.AuthFragment;
import com.drida.asrent.presentation.base.view.viewstate.AuthCastedObjectViewState;
import com.drida.asrent.presentation.base.view.viewstate.AuthViewState;
import com.drida.asrent.utils.Connection;
import com.drida.asrent.utils.IntentStarter;
import com.drida.asrent.R;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;



public class ProfileFragment extends AuthFragment<AppBarLayout, Person, ProfileView, ProfilePresenter>
        implements ProfileView {


    private Person person;

    private ProfileComponent profileComponent;

    @BindView(R.id.profileHeaderPic)
    CircleImageView profileImage;
    @BindView(R.id.name)
    TextView namePerson;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    public static final String KEY_PERSON = "com.drida.asrent.presentation.profile.ProfileFragment.PERSON";

    @Inject
    IntentStarter intentStarter;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_profile;
    }

    @Override
    public Person getData() {
        return person;
    }

    @Override
    public ProfilePresenter createPresenter() {

        return profileComponent.presenter();
    }
    

    @Override
    public AuthViewState<Person, ProfileView> createViewState() {
        return new AuthCastedObjectViewState<>();
    }

    @Override
    public void setData(Person data) {

        this.person = data;


            namePerson.setText(person.getName() + " " + person.getSurname());
            profileImage.setImageResource(R.drawable.com_facebook_profile_picture_blank_square);

            ProfileScreensAdapter adapter = new ProfileScreensAdapter(getChildFragmentManager(), person);

            List<ProfileScreen> profileScreens = new ArrayList<>();
            profileScreens.add(new ProfileScreen(ProfileScreen.TYPE_DETAILS, getString(R.string.text_tab_details)));
            profileScreens.add(new ProfileScreen(ProfileScreen.TYPE_ACCOUNT, getString(R.string.text_tab_account)));
            adapter.setScreens(profileScreens);
            viewPager.setAdapter(adapter);
            viewPager.getAdapter().notifyDataSetChanged();
            tabs.setupWithViewPager(viewPager);


    }


    @Override
    public void loadData(boolean pullToRefresh) {
        if(Connection.isConnection(getActivity())){
            presenter.loadPerson(pullToRefresh);
        }else {
            super.showError(new Throwable(""), false);
        }


    }

    @Override
    protected void injectDependencies() {
        profileComponent = DaggerProfileComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .build();

        profileComponent.inject(this);
    }


}
