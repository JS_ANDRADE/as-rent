package com.drida.asrent.presentation.persondetails;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.drida.asrent.R;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.view.BaseActivity;
import com.drida.asrent.presentation.traveldetails.TravelDetailsFragment;
import com.drida.asrent.presentation.traveldetails.TravelDetailsFragmentBuilder;

public class PersonDetailsActivity extends BaseActivity {

    public static final String KEY_PERSON  = "person";

    private Person person;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_person);

        if(savedInstanceState == null){
            person = getIntent().getParcelableExtra(KEY_PERSON);

            getSupportActionBar().setTitle(person.getName());
           PersonDetailsFragment fragment =
                    new PersonDetailsFragmentBuilder(
                            person.getId(),
                            person.getName()
                    ).build();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .commit();

        }

    }



}
