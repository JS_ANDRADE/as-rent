package com.drida.asrent.presentation.editprofile;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface EditProfileView extends MvpView {

    public void showEditForm();

    public void showError(String messageError);

    public void showLoading();

    public void editSuccesful();


}
