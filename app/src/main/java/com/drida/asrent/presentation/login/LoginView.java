package com.drida.asrent.presentation.login;


import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface LoginView extends MvpView {


    public void showLoginForm();

    public void showError();

    public void showLoading();

    public void loginSuccesful();
}
