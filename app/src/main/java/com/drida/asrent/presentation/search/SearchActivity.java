package com.drida.asrent.presentation.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.drida.asrent.R;

import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.presentation.base.view.BaseActivity;
import com.drida.asrent.utils.Connection;
import com.drida.asrent.utils.Dates;
import com.drida.asrent.utils.IntentStarter;
import com.drida.asrent.utils.Weights;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SearchActivity extends BaseActivity {

    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE_FROM = 1;
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE_TOWARD = 2;


    @BindView(R.id.from)
    EditText from;
    @BindView(R.id.toward)
    EditText toward;
    @BindView(R.id.date)
    EditText date;
    @BindView(R.id.weight)
    EditText weight;
    @BindView(R.id.weight_type)
    Spinner weightSpinner;

    @BindView(R.id.input_from)
    TextInputLayout inputFrom;
    @BindView(R.id.input_toward)
    TextInputLayout inputToward;
    @BindView(R.id.input_date)
    TextInputLayout inputDate;
    @BindView(R.id.input_weight)
    TextInputLayout inputWeight;


    @Inject
    IntentStarter intentStarter;

    SearchComponent searchComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);

        loadDataInWeightSpinner();
    }

    private void loadDataInWeightSpinner() {

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.types_weight, android.R.layout.simple_spinner_item);

        weightSpinner.setAdapter(adapter);

    }


    @OnClick(R.id.from)
    public void onFromClicked() {

        intentStarter.showAutocompletePlace(this, PLACE_AUTOCOMPLETE_REQUEST_CODE_FROM);

    }

    @OnClick(R.id.toward)
    public void onTowardClicked() {
        intentStarter.showAutocompletePlace(this, PLACE_AUTOCOMPLETE_REQUEST_CODE_TOWARD);
    }

    @OnClick(R.id.search_button)
    public void onSeachClicked() {


        if (validateData() && Connection.isConnectionWithToast(this)) {

            SearchTravel searchTravel = new SearchTravel();

            searchTravel.setFrom(from.getText().toString());
            searchTravel.setTo(toward.getText().toString());
            searchTravel.setDate(date.getText().toString());
            double weightTotal = 0;
            if(weightSpinner.getSelectedItemPosition()==1){
                weightTotal = Weights.transformKilosToGram(Double.parseDouble(weight.getText().toString()));
            }else if(weightSpinner.getSelectedItemPosition()==0){
                weightTotal = Double.parseDouble(weight.getText().toString());
            }
            searchTravel.setWeight(weightTotal);
            intentStarter.showSearchResults(this, searchTravel);
        }

    }

    private boolean validateData() {
        boolean valid = true;

        if (TextUtils.isEmpty(from.getText())) {

            inputFrom.setError(getString(R.string.error_required_field));
            valid = false;
        } else {
            inputFrom.setError(null);
        }

        if (TextUtils.isEmpty(toward.getText())) {

            inputToward.setError(getString(R.string.error_required_field));
            valid = false;
        } else {
            inputToward.setError(null);
        }

        if (TextUtils.isEmpty(date.getText())) {

            inputDate.setError(getString(R.string.error_required_field));
            valid = false;
        } else {
            inputDate.setError(null);
        }

        if (TextUtils.isEmpty(weight.getText())) {

            inputWeight.setError(getString(R.string.error_required_field));
            valid = false;
        } else {
            inputWeight.setError(null);
        }

        return valid;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_FROM) {

            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                from.setText(place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);

            } else if (resultCode == RESULT_CANCELED) {
                Log.d("ERROR", "Entro RESULT CANCELED");
            }

        }

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_TOWARD) {

            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                toward.setText(place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);

                Log.i("ERROR", "Entro Result Error");
            } else if (resultCode == RESULT_CANCELED) {
                Log.d("ERROR", "Entro RESULT CANCELED");
            }

        }

    }


    @OnClick(R.id.date)
    public void onClickedDate() {

        captureDate();
    }


    private void captureDate() {
        Dates.showDatePickerDialog(this, date);

    }


    @Override
    protected void injectDependencies() {
        searchComponent = DaggerSearchComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();

        searchComponent.inject(this);
    }
}
