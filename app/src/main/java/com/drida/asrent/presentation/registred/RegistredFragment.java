package com.drida.asrent.presentation.registred;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.drida.asrent.R;

import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.ShareApplication;

import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.presentation.base.view.BaseViewStateFragment;

import com.drida.asrent.utils.Connection;
import com.drida.asrent.utils.Dates;
import com.drida.asrent.utils.Dialogs;
import com.drida.asrent.utils.IntentStarter;
import com.drida.asrent.utils.Validations;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class RegistredFragment extends BaseViewStateFragment<RegistredView, RegistredPresenter, RegistredViewState>
        implements RegistredView {

    @Inject
    IntentStarter intentStarter;

    @BindView(R.id.registred_progress)
    ProgressBar progressBar;
    @BindView(R.id.registred_facebook_button)
    Button registredFacebookButton;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.surname)
    EditText surname;
    @BindView(R.id.genderSpinner)
    Spinner gender;
    @BindView(R.id.birthdate)
    EditText birthdate;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.password_confirm)
    EditText passwordConfirm;
    @BindView(R.id.registred_button)
    Button registredButton;
    @BindView(R.id.login_view_button)
    ViewGroup loginViewButton;
    @BindView(R.id.text_yes_account)
    TextView textYesAccount;
    @BindView(R.id.text_login)
    TextView textLogin;

    RegistredComponent registredComponent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);


    }


    private void loadDataInSexSpinner() {

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.sexs, android.R.layout.simple_spinner_item);

        if (gender == null) {
            Log.d("ERROR", "ERROR gender null");
        }
        gender.setAdapter(adapter);
    }


    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        setFormEnabled(true);

        loadDataInSexSpinner();

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_registred;
    }

    @Override
    public RegistredViewState createViewState() {
        return new RegistredViewState();
    }


    @Override
    public RegistredPresenter createPresenter() {

        return registredComponent.presenter();
    }

    @Override
    public void onNewViewStateInstance() {
        showRegistredForm();
    }

    @Override
    public void showRegistredForm() {

        viewState.setShowRegistredForm();

        progressBar.setVisibility(View.GONE);
        setFormEnabled(true);

    }

    @Override
    public void showError() {

        viewState.setShowError();

        progressBar.setVisibility(View.GONE);
        setFormEnabled(true);

        Dialogs.showDialogSimple(getActivity(), getString(R.string.error_registred), getString(R.string.error_registred_failed));
    }


    @Override
    public void showLoading() {

        progressBar.setVisibility(View.VISIBLE);
        viewState.setShowLoading();
        setFormEnabled(false);

    }

    private void setFormEnabled(boolean enabled) {

        registredFacebookButton.setEnabled(enabled);
        name.setEnabled(enabled);
        surname.setEnabled(enabled);
        gender.setEnabled(enabled);
        birthdate.setEnabled(enabled);
        email.setEnabled(enabled);
        password.setEnabled(enabled);
        passwordConfirm.setEnabled(enabled);
        registredButton.setEnabled(enabled);
        loginViewButton.setEnabled(enabled);
        textYesAccount.setEnabled(enabled);
        textLogin.setEnabled(enabled);

    }

    @Override
    public void registredSuccesfull() {

        progressBar.setVisibility(View.GONE);
        getActivity().finish();
        Log.d("ENTRO", "Entro LoginSucessFul LoginFragment");

    }

    public boolean validateData() {

        boolean valid = true;

        if (TextUtils.isEmpty(name.getText())) {
            name.setError(getString(R.string.error_required_field));
            valid = false;
        }

        if (TextUtils.isEmpty(email.getText())) {
            email.setError(getString(R.string.error_required_field));
            valid = false;
        } else {

            if (!Validations.validateEmail(email.getText().toString())) {
                email.setError(getString(R.string.error_invalid_email));
                valid = false;
            }

        }

        if (TextUtils.isEmpty(password.getText())) {
            password.setError(getString(R.string.error_required_field));
            valid = false;
        } else {
            if (Validations.validatePassword(password.getText().toString())) {
                password.setError(getString(R.string.error_invalid_password));
                valid = false;
            }
        }

        if (TextUtils.isEmpty(passwordConfirm.getText())) {

            passwordConfirm.setError(getString(R.string.error_required_field));
            valid = false;

        } else {

            if (!passwordConfirm.getText().toString().equals(password.getText().toString())) {
                password.setError(getString(R.string.error_password_they_match));
                valid = false;
            }

        }

        return valid;

    }

    @OnClick(R.id.registred_button)
    public void onRegistredClicked() {


        if (validateData()) {

            Person personRegisred = new Person();

            personRegisred.setName(name.getText().toString());
            personRegisred.setSurname(surname.getText().toString());
            if (gender.getSelectedItemPosition() != 0)
                personRegisred.setGender(gender.getSelectedItem().toString());
            if (!TextUtils.isEmpty(birthdate.getText()))
                personRegisred.setBirthdate(Dates.transformDateBaseToDateString(birthdate.getText().toString()));
            personRegisred.setEmail(email.getText().toString());
            personRegisred.setPassword(password.getText().toString());
            personRegisred.setMemberSince(Dates.getDateStringCurrentDate());
            personRegisred.setVerifiedEmail(true);
            personRegisred.setTravels(0);
            personRegisred.setRatings(0);

            presenter.doRegistred(personRegisred);
        }
    }

    @OnClick(R.id.login_view_button)
    public void onLoginViewClicked() {

        intentStarter.showAuthentication(getContext());
        getActivity().finish();
    }

    @OnClick(R.id.birthdate)
    public void onBirthdateClicked() {

        Dates.showDatePickerDialog(getActivity(), birthdate);

    }

    @Override
    protected void injectDependencies() {
        registredComponent = DaggerRegistredComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();

        registredComponent.inject(this);

    }
}
