package com.drida.asrent.presentation.profile.deatails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.drida.asrent.R;


import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.presentation.base.view.BaseFragment;
import com.drida.asrent.presentation.profile.DaggerProfileComponent;
import com.drida.asrent.presentation.profile.ProfileComponent;
import com.drida.asrent.utils.IntentStarter;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class ProfileDetailsFragment extends BaseFragment {

    @Arg
    Person person;

    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.ratings)
    TextView ratings;
    @BindView(R.id.travels)
    TextView travels;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.verified_facebook)
    TextView verifiedFacebook;
    @BindView(R.id.verified_email)
    TextView verifiedEmail;


    @Inject
    IntentStarter intentStarter;

    private ProfileComponent profileComponent;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_profile_tab_details;
    }

    @OnClick({R.id.profile_public_view})
    public void onProfilePublicClicked(){

        intentStarter.showPersonDetails(getActivity(), person);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        description.setText(person.getDescription());
        ratings.setText(String.valueOf(person.getRatings()));
        travels.setText(String.valueOf(person.getTravels()));
        phone.setText(person.getPhone());
        verifiedFacebook.setText((person.isVerifiedFacebook())? getString(R.string.text_verified) : getString(R.string.text_no_verified));
        verifiedEmail.setText(person.isVerifiedEmail() ? getString(R.string.text_verified) : getString(R.string.text_no_verified));

    }

    @Override
    protected void injectDependencies() {
        profileComponent = DaggerProfileComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();

        profileComponent.inject(this);
    }
}
