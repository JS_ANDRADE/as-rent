package com.drida.asrent.presentation.traveldetails;

import com.drida.asrent.domain.model.travel.Travel;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface TravelDetailsView extends MvpLceView<Travel> {


}


