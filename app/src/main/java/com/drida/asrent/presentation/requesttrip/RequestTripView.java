package com.drida.asrent.presentation.requesttrip;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface RequestTripView extends MvpView {

    public void showRequestTripForm();

    public void showError(String messageError);

    public void showLoading();

    public void updateSpaceAvailable(double newSpace);

    public void requestTripSuccesful();

}
