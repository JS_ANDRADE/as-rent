package com.drida.asrent.presentation.registred;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public class RegistredViewState implements ViewState<RegistredView> {

    final int STATE_SHOW_REGISTRED_FORM = 0;
    final int STATE_SHOW_LOADING = 1;
    final int STATE_SHOW_ERROR = 2;

    int state = STATE_SHOW_REGISTRED_FORM;

    public void setShowRegistredForm() {
        state = STATE_SHOW_REGISTRED_FORM;
    }

    public void setShowError() {
        state = STATE_SHOW_ERROR;
    }

    public void setShowLoading() {
        state = STATE_SHOW_LOADING;
    }


    @Override
    public void apply(RegistredView view, boolean retained) {

        switch (state) {
            case STATE_SHOW_LOADING:
                view.showLoading();
                break;

            case STATE_SHOW_ERROR:
                view.showError();
                break;

            case STATE_SHOW_REGISTRED_FORM:
                view.showRegistredForm();
                break;
        }
    }


}
