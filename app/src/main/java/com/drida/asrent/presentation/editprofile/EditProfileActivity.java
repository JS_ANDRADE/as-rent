package com.drida.asrent.presentation.editprofile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.drida.asrent.R;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.presentation.base.view.BaseViewStateActivity;
import com.drida.asrent.utils.Connection;
import com.drida.asrent.utils.Dates;
import com.drida.asrent.utils.Dialogs;
import com.drida.asrent.utils.IntentStarter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends BaseViewStateActivity<EditProfileView, EditProfilePresenter, EditProfileViewState>
        implements EditProfileView {

    public static final String KEY_PERSON = "person";


    @BindView(R.id.progress_bar)
    ProgressBar progressBar;


    @Inject
    IntentStarter intentStarter;


    @BindView(R.id.profileHeaderPic)
    CircleImageView imagePerson;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.surname)
    EditText surname;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.location)
    EditText location;
    @BindView(R.id.birthdate)
    EditText birthdate;
    @BindView(R.id.ocupation)
    EditText ocupation;
    @BindView(R.id.update_profile_button)
    Button updateProfileButton;

    private Person person;

    EditProfileComponent editProfileComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_profile);


        if (savedInstanceState == null) {


            imagePerson.setImageResource(R.drawable.com_facebook_profile_picture_blank_square);
            person = getIntent().getParcelableExtra(KEY_PERSON);
            name.setText(person.getName());
            surname.setText(person.getSurname());
            phone.setText(person.getPhone());
            description.setText(person.getDescription());
            location.setText(person.getLocation());
            birthdate.setText(Dates.transformDateStringToDateBase(person.getBirthdate()));
            ocupation.setText(person.getOcupation());

        }

        setFormEnabled(true);
    }


    @NonNull
    @Override
    public EditProfilePresenter createPresenter() {
        return editProfileComponent.presenter();
    }

    @OnClick(R.id.update_profile_button)
    public void onPostClicked() {

        if (Connection.isConnection(this)) {

            Person personUpdate = new Person();

            personUpdate.setId(person.getId());
            personUpdate.setName(name.getText().toString());
            personUpdate.setSurname(surname.getText().toString());
            personUpdate.setPhone(phone.getText().toString());
            personUpdate.setDescription(description.getText().toString());
            personUpdate.setLocation(location.getText().toString());
            personUpdate.setBirthdate(Dates.transformDateBaseToDateString(birthdate.getText().toString()));
            personUpdate.setOcupation(ocupation.getText().toString());

            presenter.doUpdatePerson(personUpdate);
        } else {
            Dialogs.showDialogSimple(this, getString(R.string.text_error), getString(R.string.text_no_connection));
        }

    }

    @OnClick(R.id.birthdate)
    public void onBirthdateClicked() {
        Dates.showDatePickerDialog(this, birthdate);
    }


    @Override
    protected void injectDependencies() {
        editProfileComponent = DaggerEditProfileComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();

        editProfileComponent.inject(this);
    }


    private boolean validateData() {

        return true;
    }

    private void setFormEnabled(boolean enabled) {

        imagePerson.setEnabled(enabled);
        name.setEnabled(enabled);
        surname.setEnabled(enabled);
        phone.setEnabled(enabled);
        birthdate.setEnabled(enabled);
        location.setEnabled(enabled);
        description.setEnabled(enabled);
        ocupation.setEnabled(enabled);
        updateProfileButton.setEnabled(enabled);


    }

    @Override
    public void showEditForm() {

        viewState.setShowEditForm();

        progressBar.setVisibility(View.GONE);

        setFormEnabled(true);

    }

    @Override
    public void showError(String messageError) {

        viewState.setShowError(messageError);

        progressBar.setVisibility(View.GONE);
        setFormEnabled(true);


        Dialogs.showDialogSimple(this, getString(R.string.text_error), messageError);

    }

    @Override
    public void showLoading() {

        progressBar.setVisibility(View.VISIBLE);
        viewState.setShowLoading();
        setFormEnabled(false);

    }

    @Override
    public void editSuccesful() {

        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, getString(R.string.ok_update_profile), Toast.LENGTH_SHORT).show();
        this.finish();

    }

    @NonNull
    @Override
    public EditProfileViewState createViewState() {
        return new EditProfileViewState();
    }

    @Override
    public void onNewViewStateInstance() {
        showEditForm();
    }

}
