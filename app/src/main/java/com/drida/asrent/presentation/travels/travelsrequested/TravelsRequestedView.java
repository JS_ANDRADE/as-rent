package com.drida.asrent.presentation.travels.travelsrequested;


import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.view.BaseTravelView;

import java.util.List;

public interface TravelsRequestedView extends BaseTravelView<List<Travel>> {


}
