

package com.drida.asrent.presentation.base.view;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MenuItem;

import butterknife.ButterKnife;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.viewstate.MvpViewStateActivity;
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;
import icepick.Icepick;


public abstract class BaseViewStateActivity<V extends MvpView, P extends MvpPresenter<V>, VS extends ViewState<V>>
    extends MvpViewStateActivity<V, P, VS> {

  @Override protected void onCreate(Bundle savedInstanceState) {
    injectDependencies();
    super.onCreate(savedInstanceState);
    Icepick.restoreInstanceState(this, savedInstanceState);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home: //hago un case por si en un futuro agrego mas opciones
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override public void onContentChanged() {
    super.onContentChanged();
    ButterKnife.bind(this);
  }

  @Override protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    Icepick.saveInstanceState(this, outState);
  }

  protected void injectDependencies() {

  }
}
