package com.drida.asrent.presentation.base.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;


import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public abstract class AuthRefreshFragment<M, V extends AuthView<M>, P extends MvpPresenter<V>>
        extends AuthFragment<SwipeRefreshLayout, M, V, P>
        implements AuthView<M>, SwipeRefreshLayout.OnRefreshListener {


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedIntanceState) {

        super.onViewCreated(view, savedIntanceState);

        contentView.setOnRefreshListener(this);


    }

    @Override
    public void showAuthenticationRequired() {
        super.showAuthenticationRequired();
        contentView.setRefreshing(false);

    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void showContent() {
        super.showContent();
        contentView.setRefreshing(false);
    }


    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.setRefreshing(false);
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);

        if (pullToRefresh && !contentView.isRefreshing()) {

            contentView.post(new Runnable() {
                @Override
                public void run() {
                        contentView.setRefreshing(true);
                }
            });

        }
    }

}
