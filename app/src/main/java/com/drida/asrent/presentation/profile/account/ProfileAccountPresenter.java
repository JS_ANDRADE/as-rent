package com.drida.asrent.presentation.profile.account;

import android.support.annotation.NonNull;

import com.drida.asrent.domain.model.account.Account;
import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.event.LoginSuccessfulEvent;
import com.drida.asrent.domain.model.event.NotAuthenticatedEvent;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ProfileAccountPresenter extends MvpBasePresenter<ProfileAccountView> {

    private AccountManager accountManager;
    private EventBus eventBus;
    private DisposableObserver<Account> subscription;

    @Inject
    public ProfileAccountPresenter(AccountManager accountManager, EventBus eventBus){
        this.accountManager = accountManager;
        this.eventBus = eventBus;
    }

    public void logOutUser(){
        ifViewAttached(new ViewAction<ProfileAccountView>() {
            @Override
            public void run(@NonNull ProfileAccountView view) {

            }
        });

        cancelSubscription();

        subscription = new DisposableObserver<Account>() {
            @Override
            public void onNext(Account account) {
                ifViewAttached(new ViewAction<ProfileAccountView>() {
                    @Override
                    public void run(@NonNull ProfileAccountView view) {
                        eventBus.post(new LoginSuccessfulEvent(account));
                    }
                });
            }

            @Override
            public void onError(Throwable e) {
                ifViewAttached(new ViewAction<ProfileAccountView>() {
                    @Override
                    public void run(@NonNull ProfileAccountView view) {

                    }
                });
            }

            @Override
            public void onComplete() {
                ifViewAttached(new ViewAction<ProfileAccountView>() {
                    @Override
                    public void run(@NonNull ProfileAccountView view) {
                        eventBus.post(new NotAuthenticatedEvent());
                    }
                });
            }
        };




        accountManager.doLogOut()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscription);

    }

    private void reloadFragments(){



    }


    private void cancelSubscription() {

        if (subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
        }

    }

    @Override
    public void attachView(ProfileAccountView view) {

    }

    @Override
    public void detachView() {
        cancelSubscription();
    }

}
