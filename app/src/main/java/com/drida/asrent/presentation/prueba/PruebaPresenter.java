package com.drida.asrent.presentation.prueba;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

public class PruebaPresenter extends MvpBasePresenter<PruebaView> {

    EventBus eventBus;
    @Inject
    PruebaPresenter(EventBus eventBus){
        this.eventBus = eventBus;
    }
}
