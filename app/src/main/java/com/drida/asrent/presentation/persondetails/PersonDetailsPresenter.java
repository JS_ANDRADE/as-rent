package com.drida.asrent.presentation.persondetails;

import com.drida.asrent.domain.model.contact.ContactsManager;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.presentation.base.presenter.BaseRxLcePresenter;

import javax.inject.Inject;

public class PersonDetailsPresenter extends BaseRxLcePresenter<PersonDetailsView, Person> {

    private ContactsManager contactsManager;

    @Inject
    public PersonDetailsPresenter(ContactsManager contactsManager){
        this.contactsManager = contactsManager;
    }

    public void loadPerson(String id){
        subscribe(contactsManager.getPerson(id), false);
    }

}
