package com.drida.asrent.presentation.profile;


import android.support.annotation.NonNull;
import android.util.Log;

import com.drida.asrent.domain.model.account.Account;
import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.contact.ContactsManager;
import com.drida.asrent.domain.model.contact.Person;

import com.drida.asrent.domain.model.event.UpdateProfileEvent;
import com.drida.asrent.presentation.base.presenter.BaseRxAuthPresenter;
import com.drida.asrent.presentation.base.presenter.BaseRxLcePresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

public class ProfilePresenter extends BaseRxAuthPresenter<ProfileView, Person> {

    private AccountManager accountManager;
    private ContactsManager contactsManager;

    @Inject
    public ProfilePresenter(ContactsManager contactsManager, AccountManager accountManager,EventBus eventBus){
        super(eventBus);
        this.contactsManager = contactsManager;
        this.accountManager = accountManager;
    }


    public void loadPerson(boolean pullToRefresh){

        Log.d("ENTRO", "Entro loadPerson con id" + accountManager.getCurrentAccount().getIdUser());

        subscribe(contactsManager.getPerson(accountManager.getCurrentAccount().getIdUser()), pullToRefresh);

    }



    @Subscribe
    public void onEventMainThread(UpdateProfileEvent event){

        ifViewAttached(true, view -> {
            Log.d("ENTRO", "Entro LoginSuccesFulEVent en BaseRxAuthPresenter");
            view.loadData(false);
        });

    }


}
