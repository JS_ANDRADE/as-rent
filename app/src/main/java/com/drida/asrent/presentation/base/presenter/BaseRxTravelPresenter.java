package com.drida.asrent.presentation.base.presenter;

import com.drida.asrent.domain.model.travel.TravelProvider;
import com.drida.asrent.presentation.base.view.BaseTravelView;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

public class BaseRxTravelPresenter<V extends BaseTravelView<M>, M>
    extends BaseRxAuthPresenter<V,M>{

    protected TravelProvider travelProvider;

    @Inject
    public BaseRxTravelPresenter(TravelProvider travelProvider, EventBus eventBus) {
        super(eventBus);

        this.travelProvider = travelProvider;
    }
    








}
