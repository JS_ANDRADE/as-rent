package com.drida.asrent.presentation.resultssearch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.drida.asrent.R;
import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.view.BaseTravelsFragment;
import com.drida.asrent.presentation.base.view.TravelsAdapter;
import com.drida.asrent.presentation.base.view.TravelsAdapterHolders;
import com.drida.asrent.utils.IntentStarter;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import javax.inject.Inject;

@FragmentWithArgs
public class ResultsSearchFragment extends BaseTravelsFragment<ResultsSearchView, ResultsSearchPresenter>
        implements ResultsSearchView, TravelsAdapter.TravelClickedListener {


    @Arg
    SearchTravel searchTravel;

    @Inject
    IntentStarter intentStarter;

    ResultsSearchComponent resultsSearchComponent;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("ENTRO", "Entro ResultsSearchFragment" + searchTravel.getFrom());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_travels_base;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedIntanceState) {
        super.onViewCreated(view, savedIntanceState);
    }



    @Override
    public ResultsSearchPresenter createPresenter() {

        return resultsSearchComponent.presenter();

    }


    @Override
    public void onTravelClicked(TravelsAdapterHolders.TravelViewHolder vh, Travel travel) {
        intentStarter.showTravelDetails(getActivity(), travel);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        Log.d("ENTRO", "Entro loadData() BaseTravelsFragment");


        presenter.load(pullToRefresh, searchTravel);
    }

    @Override
    protected void injectDependencies() {
        resultsSearchComponent = DaggerResultsSearchComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();
        resultsSearchComponent.inject(this);
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);

    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        Log.d("ENTRO", "Entro showError() BaseTravelsFragment");

    }

    @Override
    public void showContent() {
        super.showContent();
        Log.d("ENTRO", "Entro showContent() BaseTravelsFragment");

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {

        super.onStop();

    }

}
