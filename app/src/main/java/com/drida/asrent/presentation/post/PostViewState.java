package com.drida.asrent.presentation.post;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public class PostViewState implements ViewState<PostView> {

    String errorMessage;

    final int STATE_SHOW_POST_FORM = 0;
    final int STATE_SHOW_LOADING = 1;
    final int STATE_SHOW_ERROR = 2;

    int state = STATE_SHOW_POST_FORM;

    public void setShowPostForm() {
        state = STATE_SHOW_POST_FORM;
    }

    public void setShowError(String errorMessage) {
        state = STATE_SHOW_ERROR;
        this.errorMessage = errorMessage;
    }

    public void setShowLoading() {
        state = STATE_SHOW_LOADING;
    }


    @Override public void apply(PostView view, boolean retained) {

        switch (state) {
            case STATE_SHOW_LOADING:
                view.showLoading();
                break;

            case STATE_SHOW_ERROR:
                view.showError(errorMessage);
                break;

            case STATE_SHOW_POST_FORM:
                view.showPostForm();
                break;
        }
    }

}
