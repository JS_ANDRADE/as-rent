package com.drida.asrent.presentation.resultssearch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.drida.asrent.R;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.presentation.base.view.BaseActivity;

public class ResultsSearchActivity extends BaseActivity {

    public static final String KEY_SEARCH_TRAVEL = "search_travel";

    SearchTravel searchTravel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_results_seach);


        if (savedInstanceState == null) {


            searchTravel = getIntent().getParcelableExtra(KEY_SEARCH_TRAVEL);

            Log.d("ENTRO", "Entro ResultsSearchActivity" + searchTravel.getFrom());

            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new ResultsSearchFragmentBuilder(searchTravel).build()).commit();


        }

    }

}
