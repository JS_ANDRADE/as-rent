package com.drida.asrent.presentation.prueba;

import com.drida.asrent.dagger.PruebaModule;
import com.drida.asrent.dagger.ShareModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(

        modules = ShareModule.class

)
public interface PruebaComponent {

    PruebaPresenter presenter();
    void inject(PruebaFragment fragment);
}
