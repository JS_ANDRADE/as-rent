package com.drida.asrent.presentation.base.view.viewstate;

import android.os.Parcel;
import android.os.Parcelable;

import com.drida.asrent.presentation.base.view.AuthView;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.ParcelableDataLceViewState;

public class AuthCastedObjectViewState<D extends Parcelable, V extends AuthView<D>>
    extends ParcelableDataLceViewState<D, V>
    implements AuthViewState<D,V>{

    public AuthCastedObjectViewState(){

    }


    @Override
    public void apply(V view, boolean retained) {
        super.apply(view, retained);


        if(currentViewState == SHOWING_AUTHENTICATION_REQUIRED){
            view.showAuthenticationRequired();
        }else {
            super.apply(view, retained);
        }

    }

    @Override
    public void setShowingAuthenticationRequired() {
        currentViewState = SHOWING_AUTHENTICATION_REQUIRED;

        loadedData = null;
        exception = null;
    }
}
