package com.drida.asrent.presentation.travels.travelsrequested;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.drida.asrent.R;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.view.BaseTravelsFragment;
import com.drida.asrent.presentation.base.view.TravelsAdapter;
import com.drida.asrent.presentation.base.view.TravelsAdapterHolders;

import com.drida.asrent.utils.Connection;
import com.drida.asrent.utils.IntentStarter;

import javax.inject.Inject;


public class TravelsRequestedFragment extends BaseTravelsFragment<TravelsRequestedView, TravelsRequestedPresenter>
        implements TravelsRequestedView, TravelsAdapter.TravelClickedListener {


    @Inject
    IntentStarter intentStarter;

    TravelsRequestedComponent travelsRequestedComponent;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_travels_base;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedIntanceState) {
        super.onViewCreated(view, savedIntanceState);
    }

    @Override
    public TravelsRequestedPresenter createPresenter() {
        injectDependencies();
        return travelsRequestedComponent.presenter();

    }


    @Override
    public void onTravelClicked(TravelsAdapterHolders.TravelViewHolder vh, Travel travel) {
        intentStarter.showTravelDetails(getActivity(), travel);
    }

    @Override
    public void loadData(boolean pullToRefresh) {

        if(Connection.isConnection(getActivity())){
            presenter.load(pullToRefresh);
        }else {
            super.showError(new Throwable(""), false);
        }

    }

    @Override
    protected void injectDependencies() {
        travelsRequestedComponent = DaggerTravelsRequestedComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();
        travelsRequestedComponent.inject(this);
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);

    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        Log.d("ENTRO", "Entro showError() BaseTravelsFragment");

    }

    @Override
    public void showContent() {
        super.showContent();
        Log.d("ENTRO", "Entro showContent() BaseTravelsFragment");

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {

        super.onStop();

    }
}