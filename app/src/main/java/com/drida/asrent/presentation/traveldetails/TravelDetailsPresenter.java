package com.drida.asrent.presentation.traveldetails;

import android.util.Log;

import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.domain.model.travel.TravelProvider;
import com.drida.asrent.presentation.base.presenter.BaseRxLcePresenter;

import javax.inject.Inject;

public class TravelDetailsPresenter extends BaseRxLcePresenter<TravelDetailsView, Travel> {

    private TravelProvider travelProvider;

     @Inject
    public TravelDetailsPresenter(TravelProvider travelProvider){
        this.travelProvider = travelProvider;
     }

     public void loadTravel(String id){

         subscribe(travelProvider.getTravel(id), false);

     }


}
