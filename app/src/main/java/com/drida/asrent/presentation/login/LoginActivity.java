package com.drida.asrent.presentation.login;


import android.os.Bundle;
import android.support.annotation.Nullable;

import com.drida.asrent.R;
import com.drida.asrent.presentation.base.view.BaseActivity;


public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);


        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction().
                    replace(R.id.fragmentContainer, new LoginFragment()).commit();


        }

    }
}
