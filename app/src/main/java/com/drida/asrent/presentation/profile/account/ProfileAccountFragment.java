package com.drida.asrent.presentation.profile.account;

import android.util.Log;

import com.drida.asrent.R;


import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.event.NotAuthenticatedEvent;
import com.drida.asrent.presentation.base.view.BaseFragment;
import com.drida.asrent.presentation.profile.DaggerProfileComponent;
import com.drida.asrent.presentation.profile.ProfileComponent;
import com.drida.asrent.utils.IntentStarter;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import butterknife.OnClick;

@FragmentWithArgs
public class ProfileAccountFragment extends BaseFragment
        implements ProfileAccountView {

    @Inject
    IntentStarter intentStarter;
    @Inject
    ProfileAccountPresenter profileAccountPresenter;
    @Inject
    EventBus eventBus;

    private ProfileComponent profileComponent;

    @Arg
    Person person;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_profile_tab_account;
    }



    @OnClick(R.id.log_out_button)
    public void onLogOutClicked(){

        profileAccountPresenter.logOutUser();
        eventBus.post(new NotAuthenticatedEvent());

    }

    @OnClick(R.id.edit_profile_button)
    public void onEditProfileClicked(){


        Log.d("ENTRO", "ENtro editprofile clicked con person id" + person.getId());
        intentStarter.showEditProfile(getActivity(), person);

    }

    @Override
    protected void injectDependencies() {
        profileComponent = DaggerProfileComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();

        profileComponent.inject(this);
    }
}
