package com.drida.asrent.presentation.options;

import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.dagger.PruebaModule;
import com.drida.asrent.dagger.ShareAppComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {PruebaModule.class, NavigationModule.class},
        dependencies = ShareAppComponent.class
)
public interface OptionsComponent {

    //OptionsPresenter presenter();
    void inject(OptionsFragment fragment);
}
