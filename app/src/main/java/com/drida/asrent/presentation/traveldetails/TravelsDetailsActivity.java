package com.drida.asrent.presentation.traveldetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.drida.asrent.R;

import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.view.BaseActivity;
import com.drida.asrent.presentation.requesttrip.RequestTripActivity;
import com.drida.asrent.utils.IntentStarter;

import javax.inject.Inject;

public class TravelsDetailsActivity extends BaseActivity {

    public static final String KEY_TRAVEL = "travel";

    private Travel travel;

    @Inject
    IntentStarter intentStarter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_travel);

        if (savedInstanceState == null) {
            travel = getIntent().getParcelableExtra(KEY_TRAVEL);
            Person publisher = travel.getPublisher();

            getSupportActionBar().setTitle(travel.getLocationOrigin() + " - " + travel.getLocationDestination());

            TravelDetailsFragment fragment =
                    new TravelDetailsFragmentBuilder(
                            travel.getIdPublisher(),
                            travel.getId(),
                            R.drawable.ic_home_black_24dp,
                            "Nombre",
                            travel.getLocationDestination(),
                            travel.getLocationOrigin()

                    ).build();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .commit();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RequestTripActivity.REQUEST_TRIP_RESULT) {


            TravelDetailsFragment fragment =
                    new TravelDetailsFragmentBuilder(
                            travel.getIdPublisher(),
                            travel.getId(),
                            R.drawable.ic_home_black_24dp,
                            "Nombre",
                            travel.getLocationDestination(),
                            travel.getLocationOrigin()

                    ).build();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .commit();


        }

    }
}
