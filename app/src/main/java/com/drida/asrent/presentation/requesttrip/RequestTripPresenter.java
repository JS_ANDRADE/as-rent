package com.drida.asrent.presentation.requesttrip;

import android.support.annotation.NonNull;
import android.util.Log;

import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.event.UpdateRequestedTravelsEvent;
import com.drida.asrent.domain.model.travel.RequestTravel;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.domain.model.travel.TravelProvider;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class RequestTripPresenter extends MvpBasePresenter<RequestTripView> {

    private TravelProvider travelProvider;
    private AccountManager accountManager;
private EventBus eventBus;
    private DisposableObserver<Travel> subscription;

    @Inject
    public RequestTripPresenter(TravelProvider provider, AccountManager accountManager, EventBus eventBus) {
        this.eventBus = eventBus;
        this.travelProvider = provider;
        this.accountManager = accountManager;
    }

    public void requestTrip(RequestTravel travel) {

        Log.d("ENTRO", "Entro requesTrip RequesTripPResenter");

        Log.d("ENTRO", "Entro doPostTravel PostPresenter");

        ifViewAttached(new ViewAction<RequestTripView>() {
            @Override
            public void run(@NonNull RequestTripView view) {
                view.showLoading();
            }
        });

        cancelSubscription();

        subscription = new DisposableObserver<Travel>() {
            @Override
            public void onNext(Travel travel) {
                eventBus.post(new UpdateRequestedTravelsEvent());
            }

            @Override
            public void onError(Throwable e) {
                Log.d("ENTRO", "Entro onError RequestTriPresenter");
                ifViewAttached(new ViewAction<RequestTripView>() {
                    @Override
                    public void run(@NonNull RequestTripView view) {
                        view.showError(e.getMessage());
                    }
                });
            }

            @Override
            public void onComplete() {
                Log.d("ENTRO", "Entro onComplete RequestrTripPresenter");
                ifViewAttached(new ViewAction<RequestTripView>() {
                    @Override
                    public void run(@NonNull RequestTripView view) {
                        view.requestTripSuccesful();
                    }
                });
            }

        };

       travel.setUid_applicant(accountManager.getCurrentAccount().getIdUser());
        travelProvider.requestTravel(travel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscription);
        ;

    }

    private void cancelSubscription() {

        if (subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
        }

    }

    @Override
    public void attachView(RequestTripView view) {
        super.attachView(view);

    }

    @Override
    public void detachView() {

        super.detachView();

        cancelSubscription();
    }


}
