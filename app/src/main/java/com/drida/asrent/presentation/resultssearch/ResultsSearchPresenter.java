package com.drida.asrent.presentation.resultssearch;

import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.domain.model.travel.TravelProvider;
import com.drida.asrent.presentation.base.presenter.BaseRxTravelPresenter;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

public class ResultsSearchPresenter extends BaseRxTravelPresenter<ResultsSearchView, List<Travel>> {
    @Inject
    public ResultsSearchPresenter(TravelProvider travelProvider, EventBus eventBus) {
        super(travelProvider, eventBus);
    }

    public void load(boolean pullToRefresh, SearchTravel searchTravel){

        subscribe(travelProvider.getTravelsOfSearch(searchTravel), pullToRefresh);

    }

}
