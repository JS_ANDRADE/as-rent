package com.drida.asrent.presentation.editprofile;

import android.support.annotation.NonNull;
import android.util.Log;

import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.contact.ContactsManager;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.event.LoginSuccessfulEvent;
import com.drida.asrent.domain.model.event.UpdateProfileEvent;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.domain.model.travel.TravelProvider;
import com.drida.asrent.presentation.post.PostView;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class EditProfilePresenter extends MvpBasePresenter<EditProfileView> {

    private ContactsManager contactsManager;

    private DisposableObserver<Person> subscription;

    private EventBus eventBus;

    @Inject
    public EditProfilePresenter(ContactsManager contactsManager, EventBus eventBus) {
        this.contactsManager = contactsManager;
        this.eventBus = eventBus;
    }


    public void doUpdatePerson(Person person) {

        ifViewAttached(view -> view.showLoading());

        cancelSubscription();

        subscription = new DisposableObserver<Person>() {
            @Override
            public void onNext(Person person) {

                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                eventBus.post(new UpdateProfileEvent(person));
            }

            @Override
            public void onError(Throwable e) {

                ifViewAttached(view -> view.showError(e.getMessage()));
            }

            @Override
            public void onComplete() {

                ifViewAttached(view -> view.editSuccesful());
            }

        };


        contactsManager.updatePerson(person)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscription);

    }

    private void cancelSubscription() {

        if (subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
        }

    }

    @Override
    public void attachView(EditProfileView view) {
        super.attachView(view);

    }

    @Override
    public void detachView() {

        super.detachView();

        cancelSubscription();
    }

}
