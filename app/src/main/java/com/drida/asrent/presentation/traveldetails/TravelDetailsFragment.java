package com.drida.asrent.presentation.traveldetails;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.drida.asrent.R;
import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.view.BaseLceFragment;
import com.drida.asrent.presentation.requesttrip.RequestTripActivity;
import com.drida.asrent.utils.Dates;
import com.drida.asrent.utils.IntentStarter;
import com.drida.asrent.utils.Prices;
import com.drida.asrent.utils.Weights;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.ParcelableDataLceViewState;

import javax.inject.Inject;

import butterknife.BindAnim;
import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class TravelDetailsFragment extends BaseLceFragment<TextView, Travel, TravelDetailsView, TravelDetailsPresenter>
        implements TravelDetailsView {


    @Arg
    String idTravel;
    @Arg
    String travelLocationOrigin;
    @Arg
    String travelLocationDestination;
    @Arg
    String idPublisher;
    @Arg
    String publisherName;
    @Arg
    int publisherImage;

    @BindView(R.id.hour)
    TextView hour;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.location_origin)
    TextView location_origin;
    @BindView(R.id.locartion_destination)
    TextView location_destination;
    @BindView(R.id.hour_departure)
    TextView hour_departure;
    @BindView(R.id.hour_arrived)
    TextView hour_arrived;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.available_space)
    TextView availableSpace;
    @BindView(R.id.request_trip_button)
    Button requestTripButton;

    @BindView(R.id.name_publisher)
    TextView name_publisher;
    @BindView(R.id.ratings_publisher)
    TextView rating_publisher;
    @BindView(R.id.phone_publisher)
    TextView phone_pùblisher;
    @BindView(R.id.verified_facebook_publisher)
    TextView verified_facebook_publisher;

    @Inject
    IntentStarter intentStarter;

    private Travel travel;
    private TravelDetailsComponent travelDetailsComponent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutRes() {

        return R.layout.fragment_details_travel;
    }

    @Override
    public Travel getData() {
        return travel;
    }

    @Override
    public TravelDetailsPresenter createPresenter() {
        return travelDetailsComponent.presenter();
    }

    @NonNull
    @Override
    public LceViewState<Travel, TravelDetailsView> createViewState() {
        return new ParcelableDataLceViewState<>();
    }


    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return "Error Travel Details Fragment";
    }

    @Override
    public void setData(Travel data) {

        this.travel = data;
        Person publisher = travel.getPublisher();

        hour.setText(travel.getHourArrival());
        date.setText(Dates.joinDates(travel.getDateDeparture(), travel.getDateArrival()));
        location_origin.setText(travel.getLocationOrigin());
        location_destination.setText(travel.getLocationDestination());
        hour_arrived.setText(travel.getHourArrival());
        hour_departure.setText(travel.getHourDeparture());
        description.setText(travel.getDescription());
        price.setText(Prices.transformToEuros(travel.getPriceWeight()) + "/" + travel.getTypeWeight());
        availableSpace.setText(Weights.transformGramsToKilos(travel.getAvailableSpace()) + " KG");
        if (!Dates.isDateAvailable(travel.getDateDeparture(), travel.getHourDeparture()))
            requestTripButton.setEnabled(false);
        if(travel.getAvailableSpace()==0)
            requestTripButton.setEnabled(false);
        name_publisher.setText(publisher.getName());
        rating_publisher.setText(String.valueOf(publisher.getRatings()));
        phone_pùblisher.setText(publisher.getPhone());
        if (publisher.isVerifiedEmail()) {
            verified_facebook_publisher.setTextColor(getResources().getColor(R.color.green));
            verified_facebook_publisher.setText(getString(R.string.text_verified));
        } else {
            verified_facebook_publisher.setTextColor(getResources().getColor(R.color.red));
            verified_facebook_publisher.setText(getString(R.string.text_no_verified));
        }


    }

    @Override
    public void loadData(boolean pullToRefresh) {

        Log.d("ENTRO", "Entro loadTravel TravelDetailsFragment con id travel -- " + idTravel);
        presenter.loadTravel(idTravel);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



    }

    @OnClick(R.id.request_trip_button)
    public void onRequestTripClicked() {

        intentStarter.showRequestTrip(getActivity(), travel, RequestTripActivity.REQUEST_TRIP_RESULT);

    }


    @OnClick(R.id.content_publisher_view)
    public void onContentPublisherClicked() {

        Person publisher = new Person();

        publisher.setName(publisherName);
        publisher.setId(idPublisher);

        intentStarter.showPersonDetails(getActivity(), publisher);

    }


    @Override
    protected void injectDependencies() {
        travelDetailsComponent =
                DaggerTravelDetailsComponent.builder()
                        .shareAppComponent(ShareApplication.getShareComponents())
                        .navigationModule(new NavigationModule())
                        .build();

        travelDetailsComponent.inject(this);
    }
}
