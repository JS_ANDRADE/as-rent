package com.drida.asrent.presentation.requesttrip;

import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.dagger.ShareAppComponent;
import com.drida.asrent.dagger.ShareModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (
        modules = {ShareModule.class, NavigationModule.class},
        dependencies = ShareAppComponent.class
)
public interface RequestTripComponent {

    RequestTripPresenter presenter();

    void inject(RequestTripActivity activity);

}
