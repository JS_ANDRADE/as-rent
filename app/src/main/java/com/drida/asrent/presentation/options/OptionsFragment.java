package com.drida.asrent.presentation.options;

import com.drida.asrent.presentation.base.view.BaseFragment;
import com.drida.asrent.utils.IntentStarter;
import com.drida.asrent.R;
import com.drida.asrent.ShareApplication;

import com.drida.asrent.dagger.NavigationModule;


import javax.inject.Inject;

import butterknife.OnClick;

public class OptionsFragment extends BaseFragment {



    @Inject
    IntentStarter intentStarter;

    OptionsComponent optionsComponent;


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_options;
    }

    @OnClick(R.id.publish_button)
    public void onPublishClicked(){
        intentStarter.showPost(getActivity());
    }

    @OnClick(R.id.search_button)
    public void onSearchClicked(){
        intentStarter.showSearch(getActivity());
    }


    @Override
    protected void injectDependencies() {
        optionsComponent = DaggerOptionsComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();

        optionsComponent.inject(this);
    }
}
