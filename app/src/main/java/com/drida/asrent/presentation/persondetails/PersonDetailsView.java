package com.drida.asrent.presentation.persondetails;

import com.drida.asrent.domain.model.contact.Person;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface PersonDetailsView extends MvpLceView<Person>{
}
