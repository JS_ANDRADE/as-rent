package com.drida.asrent.presentation.resultssearch;

import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.view.BaseTravelView;

import java.util.List;

public interface ResultsSearchView extends BaseTravelView<List<Travel>> {
}
