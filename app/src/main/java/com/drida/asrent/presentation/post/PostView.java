package com.drida.asrent.presentation.post;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface PostView extends MvpView {

    public void showPostForm();

    public void showError(String messageError);

    public void showLoading();

    public void postSuccesful();

}
