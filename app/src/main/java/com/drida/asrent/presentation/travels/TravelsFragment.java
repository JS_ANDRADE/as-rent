package com.drida.asrent.presentation.travels;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;


import com.drida.asrent.R;

import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.domain.model.travel.TravelScreen;
import com.drida.asrent.presentation.base.view.AuthFragment;
import com.drida.asrent.presentation.base.view.BaseFragment;
import com.drida.asrent.presentation.base.view.BaseLceFragment;
import com.drida.asrent.presentation.base.view.BaseTravelsFragment;
import com.drida.asrent.presentation.base.view.TravelsAdapter;
import com.drida.asrent.presentation.base.view.TravelsAdapterHolders;
import com.drida.asrent.presentation.base.view.viewstate.AuthCastedArrayListViewState;
import com.drida.asrent.presentation.base.view.viewstate.AuthViewState;
import com.drida.asrent.utils.IntentStarter;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.CastedArrayListLceViewState;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;


public class TravelsFragment extends BaseFragment {


    private TravelsScreenAdapter adapter;


    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabs;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<TravelScreen> screens = new ArrayList<TravelScreen>();
        screens.add(new TravelScreen(TravelScreen.TYPE_PUBLISHED, getString(R.string.text_tab_published)));
        screens.add(new TravelScreen(TravelScreen.TYPE_REQUESTED, getString(R.string.text_tab_requested)));


        adapter = new TravelsScreenAdapter(getChildFragmentManager());
        adapter.setScreens(screens);
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_travels;
    }







}