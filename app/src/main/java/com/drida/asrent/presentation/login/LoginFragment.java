package com.drida.asrent.presentation.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.drida.asrent.domain.model.account.AuthCredentials;
import com.drida.asrent.presentation.base.view.BaseViewStateFragment;
import com.drida.asrent.utils.Connection;
import com.drida.asrent.utils.IntentStarter;
import com.drida.asrent.R;
import com.drida.asrent.ShareApplication;


import com.drida.asrent.utils.Dialogs;
import com.drida.asrent.utils.Validations;
import com.facebook.CallbackManager;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;


public class LoginFragment extends BaseViewStateFragment<LoginView, LoginPresenter, LoginViewState>
        implements LoginView {


    @Inject
    CallbackManager mCallBackManager;
    @Inject
    IntentStarter intentStarter;

    @BindView(R.id.sign_in_facebook_button)
    Button signInFacebookButton;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.sign_in_button)
    Button signInButton;
    @BindView(R.id.registred_view)
    ViewGroup registredView;
    @BindView(R.id.login_progress)
    ProgressBar progressBar;
    @BindView(R.id.login_form)
    ViewGroup loginForm;
    @BindView(R.id.text_no_account)
    TextView textNoAccount;
    @BindView(R.id.text_registred)
    TextView textRegistred;

    private LoginComponent loginComponent;

    public void onCreate(Bundle savedInstanceState) {
        Log.d("ENTRO", "Entro onCreate LoginFragment");
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        Log.d("ENTRO", "ENTRO onViewCreated LoginFragment ");
        super.onViewCreated(v, savedInstanceState);

        setFormEnabled(true);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("ENTRO", "ENTRO onCreatedView LoginFragment ");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public LoginViewState createViewState() {

        return new LoginViewState();
    }


    @Override
    public LoginPresenter createPresenter() {

        return loginComponent.presenter();
    }


    @OnClick(R.id.sign_in_button)
    public void onLoginClicked() {

        String mail = email.getText().toString();
        String pass = password.getText().toString();

        AuthCredentials credentials = new AuthCredentials(mail, pass);


        if (validateCredentials(credentials) && Connection.isConnectionWithToast(getActivity())) {
            presenter.doLogin(credentials);
        }

    }

    @OnClick(R.id.registred_view)
    public void onRegistredClicked() {

        intentStarter.showRegistred(getActivity());
        getActivity().finish();
    }

    private boolean validateCredentials(AuthCredentials credentials) {

        boolean valid = true;

        if (TextUtils.isEmpty(credentials.getEmail())) {
            email.setError(getString(R.string.error_required_field));
            valid = false;
        } else {
            if (!Validations.validateEmail(email.getText().toString())) {
                email.setError(getString(R.string.error_invalid_email));
                valid = false;
            }
        }

        if (TextUtils.isEmpty(credentials.getPassword())) {
            password.setError(getString(R.string.error_required_field));
            valid = false;
        } else {
            if (Validations.validatePassword(password.getText().toString())) {
                password.setError(getString(R.string.error_invalid_password));
                valid = false;
            }
        }


        return valid;

    }

    @Override
    public void onNewViewStateInstance() {
        showLoginForm();
    }

    @Override
    public void showLoginForm() {

        viewState.setShowLoginForm();

        progressBar.setVisibility(View.GONE);
        setFormEnabled(true);

    }

    @Override
    public void showError() {

        viewState.setShowError();

        progressBar.setVisibility(View.GONE);
        setFormEnabled(true);


        Dialogs.showDialogSimple(getActivity(), getString(R.string.error_login), getString(R.string.error_invalid_email_password));


    }


    @Override
    public void showLoading() {

        progressBar.setVisibility(View.VISIBLE);
        viewState.setShowLoading();
        setFormEnabled(false);

    }

    private void setFormEnabled(boolean enabled) {

        email.setEnabled(enabled);
        password.setEnabled(enabled);
        signInButton.setEnabled(enabled);
        signInFacebookButton.setEnabled(enabled);
        registredView.setEnabled(enabled);
        textNoAccount.setEnabled(enabled);
        textRegistred.setEnabled(enabled);

    }

    @Override
    public void loginSuccesful() {

        progressBar.setVisibility(View.GONE);
        getActivity().finish();
        Log.d("ENTRO", "Entro LoginSucessFul LoginFragment");

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //mCallBackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void injectDependencies() {

        loginComponent = DaggerLoginComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .build();

        loginComponent.inject(this);
    }


}
