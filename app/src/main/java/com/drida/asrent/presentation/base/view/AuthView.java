package com.drida.asrent.presentation.base.view;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface AuthView<M> extends MvpLceView<M>{

    void showAuthenticationRequired();

}
