
package com.drida.asrent.presentation.base.presenter;


import android.support.annotation.NonNull;
import android.util.Log;


import com.drida.asrent.domain.model.account.NotAuthenticatedException;
import com.drida.asrent.domain.model.event.LoginSuccessfulEvent;
import com.drida.asrent.domain.model.event.NotAuthenticatedEvent;
import com.drida.asrent.domain.model.travel.TravelProvider;

import com.drida.asrent.presentation.base.view.AuthView;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.functions.Consumer;

public class BaseRxAuthPresenter<V extends AuthView<M>, M> extends BaseRxLcePresenter<V, M> {


    protected EventBus eventBus;


    public BaseRxAuthPresenter(EventBus eventBus) {
        this.eventBus = eventBus;
    }


    @Override
    protected void onError(Throwable e, boolean pullToRefresh) {
        if (e instanceof NotAuthenticatedException) {

            eventBus.post(new NotAuthenticatedEvent());
        } else {
            super.onError(e, pullToRefresh);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(NotAuthenticatedEvent event) {

        ifViewAttached(view -> {
            view.showAuthenticationRequired();
        });

    }


    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEventMainThread(LoginSuccessfulEvent event) {

        Log.d("ENTRO", "Entro onEventMainThread en BaseRxAuthPresenter");

        System.out.print("OK");


        ifViewAttached(true, new ViewAction<V>() {
            @Override
            public void run(@NonNull V view) {
                Log.d("ENTRO", "Entro LoginSuccesFulEVent en BaseRxAuthPresenter");
                view.loadData(false);
            }
        });
    }


    @Override
    public void attachView(V view) {
        super.attachView(view);


        if (!eventBus.isRegistered(this)) {

            eventBus.register(this);
        }


    }


    @Override
    public void detachView() {


    }

    @Override
    public void destroy() {
        eventBus.unregister(this);


    }
}
