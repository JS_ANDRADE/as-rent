package com.drida.asrent.presentation.travels.travelspublished;


import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.view.BaseTravelView;

import java.util.List;

public interface TravelsPublishedView extends BaseTravelView<List<Travel>> {


}
