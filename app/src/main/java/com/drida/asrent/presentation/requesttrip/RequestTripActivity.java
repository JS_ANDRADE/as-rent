package com.drida.asrent.presentation.requesttrip;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.LoginFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.drida.asrent.R;
import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.domain.model.travel.RequestTravel;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.view.BaseViewStateActivity;
import com.drida.asrent.utils.Connection;
import com.drida.asrent.utils.Dates;
import com.drida.asrent.utils.Dialogs;
import com.drida.asrent.utils.IntentStarter;
import com.drida.asrent.utils.Locations;
import com.drida.asrent.utils.Prices;
import com.drida.asrent.utils.Weights;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;

public class RequestTripActivity extends BaseViewStateActivity<RequestTripView, RequestTripPresenter, RequestTripViewState>
        implements RequestTripView {

    public static final String KEY_TRAVEL = "travel";
    public static final int REQUEST_TRIP_RESULT = 1234;


    private Travel travel;

    @Inject
    IntentStarter intentStarter;


    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.date_hour)
    TextView dateHour;
    @BindView(R.id.route)
    TextView route;
    @BindView(R.id.weight)
    EditText weight;
    @BindView(R.id.type_weight)
    Spinner typeWeight;
    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.message_to_driver)
    TextView messageToDriver;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.weight_available)
    TextView weightAvailable;
    @BindView(R.id.request_button)
    Button requestBUtton;

    RequestTripComponent requestTripComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_a_trip_page);


        if (savedInstanceState == null) {

            travel = getIntent().getParcelableExtra(KEY_TRAVEL);

            dateHour.setText(Dates.transformDateStringtoDateFormated(travel.getDateDeparture()) + " " + getString(R.string.text_at) + " " + travel.getHourDeparture());
            route.setText(Locations.joinLocations(travel.getLocationOrigin(), travel.getLocationDestination()));
            price.setText(Prices.transformToEuros(travel.getPriceWeight()) + "/" + travel.getTypeWeight());
            weightAvailable.setText(Weights.transformGramsToKilos(travel.getAvailableSpace()) + " KG");

            loadDataInTypeWeightSpinner();

        }

    }

    @NonNull
    @Override
    public RequestTripPresenter createPresenter() {
        return requestTripComponent.presenter();
    }

    private void loadDataInTypeWeightSpinner() {

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.types_weight, android.R.layout.simple_spinner_item);

        typeWeight.setAdapter(adapter);

    }

    @OnClick(R.id.request_button)
    public void onRequestClicked() {

        Log.d("ENTRO", "Entro reqest button");

        if (validateWeight() && Connection.isConnectionWithToast(this)) {

            Log.d("ENTRO", "Entro reqest button con datos validos");
            double weightAux = 0;

            if (typeWeight.getSelectedItemPosition() == 1) {

                weightAux = Weights.transformKilosToGram(Double.parseDouble(weight.getText().toString()));

            } else {

                weightAux = Double.parseDouble(weight.getText().toString());
            }

            RequestTravel requestTravel = new RequestTravel();

            requestTravel.setUid_publisher(travel.getIdPublisher());
            requestTravel.setUid_travel(travel.getId());
            requestTravel.setSpace_requested(weightAux);

            presenter.requestTrip(requestTravel);
        }

    }


    private boolean validateWeight() {


        if (TextUtils.isEmpty(weight.getText()) || weight.getText() == null) {
            Dialogs.showDialogSimple(this, getString(R.string.text_error), getString(R.string.error_invalid_weight));
            return false;
        }
        return true;
    }


    @OnTextChanged(R.id.weight)
    public void onWeightTextChanged() {

        calculateTotal();

    }

    @OnItemSelected(R.id.type_weight)
    public void onTypeWeightSelected() {

        calculateTotal();

    }

    private void calculateTotal() {

        Log.d("ENTRO", "Entro calculateTotal");

        double availableSpace = travel.getAvailableSpace();
        String typeWeightTravel = typeWeight.getSelectedItem().toString();
        double weightTotal = 0;
        double priceTotal = travel.getPriceWeight();
        if (!TextUtils.isEmpty(weight.getText()))
            weightTotal = Double.parseDouble(weight.getText().toString());
        switch (typeWeightTravel) {
            case "KG":
                if (travel.getTypeWeight().equalsIgnoreCase("G")) {
                    weightTotal = Weights.transformKilosToGram(weightTotal);
                }


                break;
            case "G":
                if (travel.getTypeWeight().equalsIgnoreCase("KG"))
                    weightTotal = Weights.transformGramsToKilos(weightTotal);
                break;
        }


        Log.d("ENTRO", "Entro spacio total introducido: " + weightTotal);
        Log.d("ENTRO", "Entro spaio total que se ve: " + travel.getAvailableSpace());
        Log.d("ENTRO", "Entro tipo de peso travel: " + travel.getTypeWeight());
        Log.d("ENTRO", "Entro tipo de peso seleccionado: " + typeWeightTravel);
        Log.d("ENTRO", "Entro precio por peso viaje: " + travel.getPriceWeight());
        Log.d("ENTRO", "Entro precio total : " + priceTotal);

        if (weightTotal > availableSpace) {
            Dialogs.showDialogSimple(this, getString(R.string.text_error), getString(R.string.error_weight_max));
            weight.setText(null);
        } else {
            weightTotal = weightTotal * priceTotal;
            total.setText(Prices.transformToEuros(weightTotal));
        }


    }

    @Override
    protected void injectDependencies() {
        requestTripComponent = DaggerRequestTripComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();

        requestTripComponent.inject(this);
    }

    @Override
    public void showRequestTripForm() {

        viewState.setShowRequestTripForm();

        progressBar.setVisibility(View.GONE);

        setFormEnabled(true);

    }

    @Override
    public void showError(String messageError) {

        Log.d("ENTRO", "Entro showError RequestrTRIpActivityl");
        viewState.setShowError(messageError);

        progressBar.setVisibility(View.GONE);
        setFormEnabled(true);


        Dialogs.showDialogSimple(this, getString(R.string.text_error), messageError);

    }

    @Override
    public void showLoading() {
        Log.d("ENTRO", "Entro showLoading RequestrTRIpActivityl\"");
        progressBar.setVisibility(View.VISIBLE);
        viewState.setShowLoading();
        setFormEnabled(false);

    }

    public void setFormEnabled(boolean enabled) {

        weight.setEnabled(enabled);
        requestBUtton.setEnabled(enabled);
    }

    @Override
    public void updateSpaceAvailable(double newSpace) {
        Log.d("ENTRO", "Entro updateSpace RequestrTRIpActivityl\"");
        viewState.setShowUpdateSpaceAvailable(newSpace);

        weightAvailable.setText(newSpace + "sdahjsdjhdjha " + travel.getTypeWeight());

    }

    @Override
    public void requestTripSuccesful() {
        Log.d("ENTRO", "Entro requestTrupSuccesful RequestrTRIpActivityl\"");
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Viaje solicitado con exito", Toast.LENGTH_SHORT).show();
        this.finish();

    }


    @NonNull
    @Override
    public RequestTripViewState createViewState() {
        return new RequestTripViewState();
    }

    @Override
    public void onNewViewStateInstance() {

        showRequestTripForm();
    }


}
