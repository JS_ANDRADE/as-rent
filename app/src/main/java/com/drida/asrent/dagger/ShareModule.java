package com.drida.asrent.dagger;

import com.drida.asrent.data.repository.AccountRepository;
import com.drida.asrent.data.repository.PersonRepository;
import com.drida.asrent.data.repository.TravelRepository;
import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.account.DefaultAccountManager;

import com.drida.asrent.domain.model.contact.ContactsManager;
import com.drida.asrent.domain.model.contact.DefaultContactsManager;

import com.facebook.CallbackManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ShareModule {

    // private final ShareApplication shareApplication;

    /*public ShareModule(ShareApplication shareApplication){

        this.shareApplication = shareApplication;

    }*/

    //private static AccountManager accountManager = new DefaultAccountManager();


    @Singleton
    @Provides
    public AccountManager providesAccountManager(DefaultAccountManager accountManager) {

        return accountManager;

    }

    @Singleton
    @Provides
    public CallbackManager provideCallbackManager() {

        return CallbackManager.Factory.create();
    }

    @Provides
    @Singleton
    public FirebaseAuth provideFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }


    @Provides
    @Singleton
    public FirebaseDatabase provideFirebaseDatabase() {
        return FirebaseDatabase.getInstance();
    }


    @Provides
    @Named("repository_account")
    @Singleton
    public AccountRepository provideRepositoryAccount(AccountRepository accountRepository){
        return accountRepository;
    }

    @Provides
    @Named("repository_person")
    @Singleton
    public PersonRepository provideRepositoryPerson(PersonRepository personRepository){
        return personRepository;
    }

    @Provides
    @Named("repository_travel")
    @Singleton
    public TravelRepository provideRepositoryTravel(TravelRepository travelRepository){
        return travelRepository;
    }


    @Singleton
    @Provides
    public EventBus providesEventBus() {
        return EventBus.getDefault();
    }

    @Singleton
    @Provides
    public ContactsManager providesContactsManager(DefaultContactsManager defaultContactsManager){
        return defaultContactsManager;
    }

}
